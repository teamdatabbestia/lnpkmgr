USE [lunaparkdb]
GO
/****** Object:  UserDefinedTableType [dbo].[IDTableType]    Script Date: 10/07/2018 17:06:02 ******/
CREATE TYPE [dbo].[IDTableType] AS TABLE(
	[ID] [int] NULL
)
GO
/****** Object:  UserDefinedFunction [dbo].[CheckClassUsesConsistency]    Script Date: 10/07/2018 17:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[CheckClassUsesConsistency] (@TicketClassID int)
RETURNS BIT
AS
BEGIN
	RETURN
	CASE
		WHEN NOT EXISTS (SELECT * FROM TICKETS WHERE TicketClassID = @TicketClassID AND dbo.CheckTicketUsesConsistency(TicketID) = 0) THEN 1
		ELSE 0
	END
END;
GO
/****** Object:  UserDefinedFunction [dbo].[CheckContractsConsistency]    Script Date: 10/07/2018 17:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[CheckContractsConsistency] (@EmployeeID int)
RETURNS BIT
AS
BEGIN
	RETURN
	CASE
		WHEN (SELECT COUNT(*) 
				FROM CONTRACTS C
				WHERE C.EmployeeID = @EmployeeID
				AND EXISTS (SELECT *
							FROM CONTRACTS C2
							WHERE C2.EmployeeID = C.EmployeeID
							AND C.StartDate != C2.StartDate
							AND NOT ( (C2.StartDate < C.StartDate AND C2.EndDate IS NOT NULL AND C2.EndDate < C.StartDate)
									OR (C.EndDate IS NOT NULL AND C2.StartDate > C.EndDate)
									)
							)
			) = 0 THEN 1
		ELSE 0
	END
END;




GO
/****** Object:  UserDefinedFunction [dbo].[CheckDateConsistency]    Script Date: 10/07/2018 17:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[CheckDateConsistency] (@StartDate date, @EndDate date)
RETURNS BIT
AS
BEGIN
	RETURN
	CASE 
		WHEN (@StartDate <= @EndDate) THEN 1
		ELSE 0
	END
END;
GO
/****** Object:  UserDefinedFunction [dbo].[CheckNullableDateConsistency]    Script Date: 10/07/2018 17:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[CheckNullableDateConsistency](@startDate DATE, @endDate DATE)
RETURNS BIT
AS
BEGIN
	RETURN
	CASE	
		WHEN (@endDate IS NULL) OR (@endDate >= @startDate) THEN 1
		ELSE 0
	END
END;
GO
/****** Object:  UserDefinedFunction [dbo].[CheckTicketUsesConsistency]    Script Date: 10/07/2018 17:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[CheckTicketUsesConsistency] (@TicketID int)
RETURNS BIT
AS
BEGIN
	RETURN
	CASE
		WHEN (SELECT COUNT(*) 
				FROM dbo.USE_DATES UD
				WHERE UD.TicketID = @TicketID) = 
			 (SELECT TC.MaxUses - T.UsesLeft
				FROM dbo.TICKETS T, dbo.TICKET_CLASSES TC
				WHERE T.TicketID = @TicketID AND T.TicketClassID = TC.TicketClassID) THEN 1
		ELSE 0
	END
END;
GO
/****** Object:  UserDefinedFunction [dbo].[EquTaskCheck]    Script Date: 10/07/2018 17:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[EquTaskCheck] (@TaskID int)
RETURNS BIT
AS
BEGIN
	RETURN
	CASE 
		WHEN exists(select * from WORKS
                 where WORKS.TaskID = @TaskID) THEN 1
		ELSE 0
	END
END;
GO
/****** Object:  Table [dbo].[ACCOUNTS]    Script Date: 10/07/2018 17:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACCOUNTS](
	[UserName] [varchar](20) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[PasswordHash] [varchar](64) NOT NULL,
 CONSTRAINT [IDUSER] PRIMARY KEY CLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [FKLogin_ID] UNIQUE NONCLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ADJECENCIES]    Script Date: 10/07/2018 17:06:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ADJECENCIES](
	[NodeB] [int] NOT NULL,
	[NodeA] [int] NOT NULL,
 CONSTRAINT [IDAdjecency] PRIMARY KEY CLUSTERED 
(
	[NodeB] ASC,
	[NodeA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BUILDING_CONTRACTS]    Script Date: 10/07/2018 17:06:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BUILDING_CONTRACTS](
	[CompanyID] [int] NOT NULL,
	[PlaceID] [int] NOT NULL,
	[BuildStart] [date] NOT NULL,
	[BuildEnd] [date] NULL,
 CONSTRAINT [IDBuildingContract] PRIMARY KEY CLUSTERED 
(
	[CompanyID] ASC,
	[PlaceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CONCESSIONS]    Script Date: 10/07/2018 17:06:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CONCESSIONS](
	[PlaceID] [int] NOT NULL,
	[CompanyID] [int] NOT NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NULL,
 CONSTRAINT [IDCONCESSION] PRIMARY KEY CLUSTERED 
(
	[CompanyID] ASC,
	[PlaceID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CONTRACT_TYPES]    Script Date: 10/07/2018 17:06:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CONTRACT_TYPES](
	[ContractType] [varchar](30) NOT NULL,
	[HoursPerWeek] [int] NOT NULL,
	[DefaultSalary] [float] NOT NULL,
 CONSTRAINT [IDCONTRACT_TYPE] PRIMARY KEY CLUSTERED 
(
	[ContractType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CONTRACTS]    Script Date: 10/07/2018 17:06:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CONTRACTS](
	[EmployeeID] [int] NOT NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NULL,
	[ContractType] [varchar](30) NOT NULL,
	[Salary] [float] NOT NULL,
 CONSTRAINT [IDCONTRACT] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EMPLOYEES]    Script Date: 10/07/2018 17:06:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EMPLOYEES](
	[EmployeeID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) NOT NULL,
	[Surname] [varchar](20) NOT NULL,
	[Email] [varchar](30) NOT NULL,
	[Gender] [char](1) NOT NULL,
	[BirthDate] [date] NOT NULL,
	[TelephoneNumber] [varchar](11) NOT NULL,
	[Role] [varchar](30) NOT NULL,
 CONSTRAINT [IDEMPLOYEE] PRIMARY KEY NONCLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IXRoleEmployee]    Script Date: 10/07/2018 17:06:08 ******/
CREATE CLUSTERED INDEX [IXRoleEmployee] ON [dbo].[EMPLOYEES]
(
	[Role] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EXTERNAL_COMPANIES]    Script Date: 10/07/2018 17:06:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EXTERNAL_COMPANIES](
	[CompanyID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Representative] [varchar](40) NOT NULL,
	[TelephoneNumber] [varchar](11) NOT NULL,
 CONSTRAINT [IDEXTERNAL_COMPANY] PRIMARY KEY CLUSTERED 
(
	[CompanyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FAULTS]    Script Date: 10/07/2018 17:06:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FAULTS](
	[FaultID] [int] IDENTITY(1,1) NOT NULL,
	[DangerLevel] [int] NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[StartDate] [date] NOT NULL,
	[SolvedDate] [date] NULL,
	[PlaceID] [int] NOT NULL,
 CONSTRAINT [IDFAULT] PRIMARY KEY NONCLUSTERED 
(
	[FaultID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IXDangerFault]    Script Date: 10/07/2018 17:06:09 ******/
CREATE CLUSTERED INDEX [IXDangerFault] ON [dbo].[FAULTS]
(
	[DangerLevel] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NODES]    Script Date: 10/07/2018 17:06:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NODES](
	[NodeID] [int] IDENTITY(1,1) NOT NULL,
	[PosX] [float] NOT NULL,
	[PosY] [float] NOT NULL,
 CONSTRAINT [IDNODE] PRIMARY KEY CLUSTERED 
(
	[NodeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PLACE_CATEGORIES]    Script Date: 10/07/2018 17:06:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PLACE_CATEGORIES](
	[Name] [varchar](30) NOT NULL,
	[Description] [varchar](200) NOT NULL,
 CONSTRAINT [IDPLACE_CATEGORIES] PRIMARY KEY CLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PLACES]    Script Date: 10/07/2018 17:06:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PLACES](
	[PlaceID] [int] IDENTITY(1,1) NOT NULL,
	[PosX] [float] NOT NULL,
	[PosY] [float] NOT NULL,
	[Name] [varchar](30) NOT NULL,
	[Width] [float] NOT NULL,
	[Height] [float] NOT NULL,
	[Available] [char](1) NOT NULL,
	[Entrance] [int] NOT NULL,
	[PlaceCategory] [varchar](30) NOT NULL,
	[Description] [varchar](200) NULL,
 CONSTRAINT [IDPLACE] PRIMARY KEY CLUSTERED 
(
	[PlaceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PURCHASES]    Script Date: 10/07/2018 17:06:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PURCHASES](
	[TicketID] [int] NOT NULL,
	[SubscriberID] [int] NOT NULL,
 CONSTRAINT [IDPurchase] PRIMARY KEY CLUSTERED 
(
	[SubscriberID] ASC,
	[TicketID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ROLES]    Script Date: 10/07/2018 17:06:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ROLES](
	[Name] [varchar](30) NOT NULL,
	[Description] [varchar](200) NOT NULL,
 CONSTRAINT [IDROLES] PRIMARY KEY CLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SCHEDULES]    Script Date: 10/07/2018 17:06:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SCHEDULES](
	[PlaceID] [int] NOT NULL,
	[Weekday] [varchar](10) NOT NULL,
	[OpeningTime] [time](7) NOT NULL,
	[ClosingTime] [time](7) NOT NULL,
 CONSTRAINT [IDSCHEDULE] PRIMARY KEY CLUSTERED 
(
	[PlaceID] ASC,
	[Weekday] ASC,
	[OpeningTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SCHEDULES_ALTERED]    Script Date: 10/07/2018 17:06:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SCHEDULES_ALTERED](
	[PlaceID] [int] NOT NULL,
	[Date] [date] NOT NULL,
	[OpeningTime] [time](7) NOT NULL,
	[ClosingTime] [time](7) NOT NULL,
 CONSTRAINT [IDSCHEDULE_ALTERED] PRIMARY KEY CLUSTERED 
(
	[PlaceID] ASC,
	[Date] ASC,
	[OpeningTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SUBSCRIBERS]    Script Date: 10/07/2018 17:06:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SUBSCRIBERS](
	[SubscriberID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) NOT NULL,
	[Surname] [varchar](20) NOT NULL,
	[Email] [varchar](30) NOT NULL,
	[Gender] [char](1) NOT NULL,
	[BirthDate] [date] NOT NULL,
	[SubscriptionDate] [date] NOT NULL,
 CONSTRAINT [IDSUBSCRIBER] PRIMARY KEY CLUSTERED 
(
	[SubscriberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TASKS]    Script Date: 10/07/2018 17:06:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TASKS](
	[TaskID] [int] IDENTITY(1,1) NOT NULL,
	[TaskDescription] [varchar](500) NOT NULL,
	[Priority] [int] NOT NULL,
	[PlaceID] [int] NULL,
	[FaultID] [int] NULL,
 CONSTRAINT [IDTASK] PRIMARY KEY NONCLUSTERED 
(
	[TaskID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IXPriorityTask]    Script Date: 10/07/2018 17:06:13 ******/
CREATE CLUSTERED INDEX [IXPriorityTask] ON [dbo].[TASKS]
(
	[Priority] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TICKET_CLASSES]    Script Date: 10/07/2018 17:06:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TICKET_CLASSES](
	[TicketClassID] [int] IDENTITY(1,1) NOT NULL,
	[Price] [float] NOT NULL,
	[Type] [varchar](30) NOT NULL,
	[Duration] [varchar](20) NOT NULL,
	[MaxUses] [int] NOT NULL,
 CONSTRAINT [IDTICKET_CLASS] PRIMARY KEY CLUSTERED 
(
	[TicketClassID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TICKETS]    Script Date: 10/07/2018 17:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TICKETS](
	[TicketID] [int] IDENTITY(1,1) NOT NULL,
	[TicketClassID] [int] NOT NULL,
	[UsesLeft] [int] NOT NULL,
 CONSTRAINT [IDTICKET] PRIMARY KEY CLUSTERED 
(
	[TicketID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[USE_DATES]    Script Date: 10/07/2018 17:06:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[USE_DATES](
	[TicketID] [int] NOT NULL,
	[UseDate] [date] NOT NULL,
 CONSTRAINT [IDUSE_DATE] PRIMARY KEY CLUSTERED 
(
	[TicketID] ASC,
	[UseDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VACATIONS]    Script Date: 10/07/2018 17:06:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VACATIONS](
	[EmployeeID] [int] NOT NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
	[Paid] [bit] NOT NULL,
 CONSTRAINT [IDVACATION] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WORKS]    Script Date: 10/07/2018 17:06:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WORKS](
	[EmployeeID] [int] NOT NULL,
	[TaskID] [int] NOT NULL,
 CONSTRAINT [IDWORK] PRIMARY KEY CLUSTERED 
(
	[TaskID] ASC,
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IXPlaceCategory]    Script Date: 10/07/2018 17:06:16 ******/
CREATE NONCLUSTERED INDEX [IXPlaceCategory] ON [dbo].[PLACES]
(
	[PlaceCategory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IXSubscribersEmail]    Script Date: 10/07/2018 17:06:16 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IXSubscribersEmail] ON [dbo].[SUBSCRIBERS]
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ACCOUNTS]  WITH CHECK ADD  CONSTRAINT [FKLogin_FK] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[EMPLOYEES] ([EmployeeID])
GO
ALTER TABLE [dbo].[ACCOUNTS] CHECK CONSTRAINT [FKLogin_FK]
GO
ALTER TABLE [dbo].[ADJECENCIES]  WITH CHECK ADD  CONSTRAINT [FKAdj_NOD] FOREIGN KEY([NodeB])
REFERENCES [dbo].[NODES] ([NodeID])
GO
ALTER TABLE [dbo].[ADJECENCIES] CHECK CONSTRAINT [FKAdj_NOD]
GO
ALTER TABLE [dbo].[ADJECENCIES]  WITH CHECK ADD  CONSTRAINT [FKLeft] FOREIGN KEY([NodeA])
REFERENCES [dbo].[NODES] ([NodeID])
GO
ALTER TABLE [dbo].[ADJECENCIES] CHECK CONSTRAINT [FKLeft]
GO
ALTER TABLE [dbo].[BUILDING_CONTRACTS]  WITH CHECK ADD  CONSTRAINT [FKR] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[EXTERNAL_COMPANIES] ([CompanyID])
GO
ALTER TABLE [dbo].[BUILDING_CONTRACTS] CHECK CONSTRAINT [FKR]
GO
ALTER TABLE [dbo].[BUILDING_CONTRACTS]  WITH CHECK ADD  CONSTRAINT [FKR_1] FOREIGN KEY([PlaceID])
REFERENCES [dbo].[PLACES] ([PlaceID])
GO
ALTER TABLE [dbo].[BUILDING_CONTRACTS] CHECK CONSTRAINT [FKR_1]
GO
ALTER TABLE [dbo].[CONCESSIONS]  WITH CHECK ADD  CONSTRAINT [FKAgreement] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[EXTERNAL_COMPANIES] ([CompanyID])
GO
ALTER TABLE [dbo].[CONCESSIONS] CHECK CONSTRAINT [FKAgreement]
GO
ALTER TABLE [dbo].[CONCESSIONS]  WITH CHECK ADD  CONSTRAINT [FKUnderContract] FOREIGN KEY([PlaceID])
REFERENCES [dbo].[PLACES] ([PlaceID])
GO
ALTER TABLE [dbo].[CONCESSIONS] CHECK CONSTRAINT [FKUnderContract]
GO
ALTER TABLE [dbo].[CONTRACTS]  WITH CHECK ADD  CONSTRAINT [FKCType] FOREIGN KEY([ContractType])
REFERENCES [dbo].[CONTRACT_TYPES] ([ContractType])
GO
ALTER TABLE [dbo].[CONTRACTS] CHECK CONSTRAINT [FKCType]
GO
ALTER TABLE [dbo].[CONTRACTS]  WITH CHECK ADD  CONSTRAINT [FKHiring] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[EMPLOYEES] ([EmployeeID])
GO
ALTER TABLE [dbo].[CONTRACTS] CHECK CONSTRAINT [FKHiring]
GO
ALTER TABLE [dbo].[EMPLOYEES]  WITH CHECK ADD  CONSTRAINT [FKR_3] FOREIGN KEY([Role])
REFERENCES [dbo].[ROLES] ([Name])
GO
ALTER TABLE [dbo].[EMPLOYEES] CHECK CONSTRAINT [FKR_3]
GO
ALTER TABLE [dbo].[FAULTS]  WITH CHECK ADD  CONSTRAINT [FKSource] FOREIGN KEY([PlaceID])
REFERENCES [dbo].[PLACES] ([PlaceID])
GO
ALTER TABLE [dbo].[FAULTS] CHECK CONSTRAINT [FKSource]
GO
ALTER TABLE [dbo].[PLACES]  WITH CHECK ADD  CONSTRAINT [FKEntrance] FOREIGN KEY([Entrance])
REFERENCES [dbo].[NODES] ([NodeID])
GO
ALTER TABLE [dbo].[PLACES] CHECK CONSTRAINT [FKEntrance]
GO
ALTER TABLE [dbo].[PLACES]  WITH CHECK ADD  CONSTRAINT [FKR_4] FOREIGN KEY([PlaceCategory])
REFERENCES [dbo].[PLACE_CATEGORIES] ([Name])
GO
ALTER TABLE [dbo].[PLACES] CHECK CONSTRAINT [FKR_4]
GO
ALTER TABLE [dbo].[PURCHASES]  WITH CHECK ADD  CONSTRAINT [FKPur_SUB] FOREIGN KEY([SubscriberID])
REFERENCES [dbo].[SUBSCRIBERS] ([SubscriberID])
GO
ALTER TABLE [dbo].[PURCHASES] CHECK CONSTRAINT [FKPur_SUB]
GO
ALTER TABLE [dbo].[PURCHASES]  WITH CHECK ADD  CONSTRAINT [FKPur_TIC] FOREIGN KEY([TicketID])
REFERENCES [dbo].[TICKETS] ([TicketID])
GO
ALTER TABLE [dbo].[PURCHASES] CHECK CONSTRAINT [FKPur_TIC]
GO
ALTER TABLE [dbo].[SCHEDULES]  WITH CHECK ADD FOREIGN KEY([PlaceID])
REFERENCES [dbo].[PLACES] ([PlaceID])
GO
ALTER TABLE [dbo].[SCHEDULES_ALTERED]  WITH CHECK ADD FOREIGN KEY([PlaceID])
REFERENCES [dbo].[PLACES] ([PlaceID])
GO
ALTER TABLE [dbo].[TASKS]  WITH CHECK ADD  CONSTRAINT [FKLocation] FOREIGN KEY([PlaceID])
REFERENCES [dbo].[PLACES] ([PlaceID])
GO
ALTER TABLE [dbo].[TASKS] CHECK CONSTRAINT [FKLocation]
GO
ALTER TABLE [dbo].[TASKS]  WITH CHECK ADD  CONSTRAINT [FKR_5] FOREIGN KEY([FaultID])
REFERENCES [dbo].[FAULTS] ([FaultID])
GO
ALTER TABLE [dbo].[TASKS] CHECK CONSTRAINT [FKR_5]
GO
ALTER TABLE [dbo].[TICKETS]  WITH CHECK ADD  CONSTRAINT [FKTClass] FOREIGN KEY([TicketClassID])
REFERENCES [dbo].[TICKET_CLASSES] ([TicketClassID])
GO
ALTER TABLE [dbo].[TICKETS] CHECK CONSTRAINT [FKTClass]
GO
ALTER TABLE [dbo].[USE_DATES]  WITH CHECK ADD  CONSTRAINT [FKUtilization] FOREIGN KEY([TicketID])
REFERENCES [dbo].[TICKETS] ([TicketID])
GO
ALTER TABLE [dbo].[USE_DATES] CHECK CONSTRAINT [FKUtilization]
GO
ALTER TABLE [dbo].[VACATIONS]  WITH CHECK ADD  CONSTRAINT [FKHasVacation] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[EMPLOYEES] ([EmployeeID])
GO
ALTER TABLE [dbo].[VACATIONS] CHECK CONSTRAINT [FKHasVacation]
GO
ALTER TABLE [dbo].[WORKS]  WITH CHECK ADD  CONSTRAINT [FKWor_EMP] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[EMPLOYEES] ([EmployeeID])
GO
ALTER TABLE [dbo].[WORKS] CHECK CONSTRAINT [FKWor_EMP]
GO
ALTER TABLE [dbo].[WORKS]  WITH CHECK ADD  CONSTRAINT [FKWor_TAS] FOREIGN KEY([TaskID])
REFERENCES [dbo].[TASKS] ([TaskID])
GO
ALTER TABLE [dbo].[WORKS] CHECK CONSTRAINT [FKWor_TAS]
GO
ALTER TABLE [dbo].[ADJECENCIES]  WITH CHECK ADD CHECK  (([NodeA]<>[NodeB]))
GO
ALTER TABLE [dbo].[BUILDING_CONTRACTS]  WITH CHECK ADD CHECK  (([dbo].[CheckNullableDateConsistency]([BuildStart],[BuildEnd])=(1)))
GO
ALTER TABLE [dbo].[CONCESSIONS]  WITH CHECK ADD CHECK  (([dbo].[CheckNullableDateConsistency]([StartDate],[EndDate])=(1)))
GO
ALTER TABLE [dbo].[CONTRACTS]  WITH CHECK ADD CHECK  (([dbo].[CheckNullableDateConsistency]([StartDate],[EndDate])=(1)))
GO
ALTER TABLE [dbo].[CONTRACTS]  WITH CHECK ADD CHECK  (([dbo].[CheckContractsConsistency]([EmployeeID])=(1)))
GO
ALTER TABLE [dbo].[EMPLOYEES]  WITH CHECK ADD CHECK  (([Email] like '%_@__%.__%'))
GO
ALTER TABLE [dbo].[EMPLOYEES]  WITH CHECK ADD CHECK  (([Gender]='E' OR [Gender]='F' OR [Gender]='M'))
GO
ALTER TABLE [dbo].[EMPLOYEES]  WITH CHECK ADD CHECK  ((NOT [TelephoneNumber] like '%[^0-9]%'))
GO
ALTER TABLE [dbo].[EMPLOYEES]  WITH CHECK ADD CHECK  ((NOT [TelephoneNumber] like '%[^0-9]%'))
GO
ALTER TABLE [dbo].[EXTERNAL_COMPANIES]  WITH CHECK ADD  CONSTRAINT [CK__EXTERNAL___Telep__02C769E9] CHECK  ((NOT [TelephoneNumber] like '%[^0-9]%'))
GO
ALTER TABLE [dbo].[EXTERNAL_COMPANIES] CHECK CONSTRAINT [CK__EXTERNAL___Telep__02C769E9]
GO
ALTER TABLE [dbo].[FAULTS]  WITH CHECK ADD CHECK  (([dbo].[CheckNullableDateConsistency]([StartDate],[SolvedDate])=(1)))
GO
ALTER TABLE [dbo].[SCHEDULES]  WITH CHECK ADD CHECK  (([OpeningTime]<[ClosingTime]))
GO
ALTER TABLE [dbo].[SCHEDULES_ALTERED]  WITH CHECK ADD CHECK  (([OpeningTime]<[ClosingTime]))
GO
ALTER TABLE [dbo].[SUBSCRIBERS]  WITH CHECK ADD CHECK  (([Email] like '%_@__%.__%'))
GO
ALTER TABLE [dbo].[SUBSCRIBERS]  WITH CHECK ADD CHECK  (([Gender]='E' OR [Gender]='F' OR [Gender]='M'))
GO
ALTER TABLE [dbo].[TICKET_CLASSES]  WITH CHECK ADD CHECK  (([dbo].[CheckClassUsesConsistency]([TicketClassID])=(1)))
GO
ALTER TABLE [dbo].[TICKETS]  WITH CHECK ADD  CONSTRAINT [Check_Ticket_Use_Left_Consistency] CHECK  (([dbo].[CheckTicketUsesConsistency]([TicketID])=(1)))
GO
ALTER TABLE [dbo].[TICKETS] CHECK CONSTRAINT [Check_Ticket_Use_Left_Consistency]
GO
ALTER TABLE [dbo].[USE_DATES]  WITH CHECK ADD  CONSTRAINT [Check_Use_Date_Consistency] CHECK  (([dbo].[CheckTicketUsesConsistency]([TicketID])=(1)))
GO
ALTER TABLE [dbo].[USE_DATES] CHECK CONSTRAINT [Check_Use_Date_Consistency]
GO
ALTER TABLE [dbo].[VACATIONS]  WITH CHECK ADD CHECK  (([dbo].[CheckDateConsistency]([StartDate],[EndDate])=(1)))
GO
/****** Object:  StoredProcedure [dbo].[AssignTaskToEmployee]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AssignTaskToEmployee]
@EmpID int,
@TaskID int
AS
INSERT INTO WORKS
VALUES (@EmpID, @TaskID)

GO
/****** Object:  StoredProcedure [dbo].[CompleteTask]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CompleteTask]
@TaskID int,
@EmployeeID int
AS
SET XACT_ABORT ON;
BEGIN TRANSACTION completeTask
DELETE FROM WORKS
WHERE WORKS.TaskID = @TaskID AND WORKS.EmployeeID = @EmployeeID;

IF NOT EXISTS(SELECT * FROM WORKS WHERE WORKS.TaskID = @TaskID)
BEGIN
	DELETE FROM TASKS
	WHERE TASKS.TaskID = @TaskID;
END
COMMIT TRANSACTION completeTask
GO
/****** Object:  StoredProcedure [dbo].[CreateNewTask]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateNewTask] 
@TaskDescription varchar(500),
@Priority int,
@PlaceId int = NULL,
@FaultID int = NULL,
@NewId int OUTPUT
AS

DECLARE @TaskIDTable IDTableType 

INSERT INTO TASKS
OUTPUT Inserted.TaskID INTO @TaskIDTable
VALUES (@TaskDescription, @Priority, @PlaceID, @FaultID)

SET @NewId = (SELECT * FROM @TaskIDTable );

GO
/****** Object:  StoredProcedure [dbo].[DeleteTimetable]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[DeleteTimetable]
@PlaceID int,
@Weekday varchar(10),
@OpeningTime time
AS



DELETE FROM [dbo].[SCHEDULES]
     WHERE
           PlaceID = @PlaceID
     AND   [Weekday] = @Weekday
     AND   OpeningTime = @OpeningTime



GO
/****** Object:  StoredProcedure [dbo].[DeleteTimetableVariation]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[DeleteTimetableVariation]
@PlaceID int,
@Date date,
@OpeningTime time
AS

DELETE FROM [dbo].[SCHEDULES_ALTERED]
     WHERE
           PlaceID = @PlaceID
     AND   [Date] = @Date
     AND   OpeningTime = @OpeningTime

GO
/****** Object:  StoredProcedure [dbo].[FireEmployee]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FireEmployee]
@EmployeeID int
AS

SET XACT_ABORT ON
BEGIN TRANSACTION fireEmp

DELETE FROM VACATIONS
WHERE EmployeeID = @EmployeeID;

DELETE FROM WORKS
WHERE EmployeeID = @EmployeeID;

UPDATE CONTRACTS
SET EndDate = CONVERT (date, GETDATE())
WHERE EmployeeID = @EmployeeID
AND (EndDate IS NULL OR EndDate >= CONVERT (date, GETDATE()))
AND StartDate <= CONVERT (date, GETDATE())


DELETE FROM CONTRACTS
WHERE EmployeeID = @EmployeeID
AND StartDate > CONVERT (date, GETDATE())


COMMIT TRANSACTION fireEmp
GO
/****** Object:  StoredProcedure [dbo].[GetAvailableEmployeesByRole]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAvailableEmployeesByRole]
@Role varchar(30),
@WorkDate date
AS
SELECT E.*
FROM EMPLOYEES E
WHERE NOT EXISTS (
	SELECT *
	FROM VACATIONS V
	WHERE (@WorkDate BETWEEN V.StartDate AND V.EndDate)
	AND V.EmployeeID = E.EmployeeID)
AND (@Role IS NULL OR E.Role = @Role)
AND EXISTS (
	SELECT *
	FROM CONTRACTS C
	WHERE C.EmployeeID = E.EmployeeID
	AND C.StartDate <= @WorkDate
	AND (C.EndDate IS NULL OR C.EndDate >= @WorkDate)
)
GO
/****** Object:  StoredProcedure [dbo].[GetBuildingContracts]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBuildingContracts]
AS
SELECT EC.Name AS Company, P.Name AS Place, BC.BuildStart, BC.BuildEnd
FROM EXTERNAL_COMPANIES EC INNER JOIN BUILDING_CONTRACTS BC
ON EC.CompanyID = BC.CompanyID
INNER JOIN PLACES P
ON P.PlaceID = BC.PlaceID
GO
/****** Object:  StoredProcedure [dbo].[GetCompanies]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCompanies]
AS
SELECT *
FROM EXTERNAL_COMPANIES
GO
/****** Object:  StoredProcedure [dbo].[GetConcessions]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetConcessions]
AS
SELECT EC.Name AS Company, P.Name AS Place, C.StartDate, C.EndDate
FROM EXTERNAL_COMPANIES EC INNER JOIN CONCESSIONS C
ON EC.CompanyID = C.CompanyID
INNER JOIN PLACES P
ON P.PlaceID = C.PlaceID
GO
/****** Object:  StoredProcedure [dbo].[GetContractTypes]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetContractTypes]
AS
SELECT *
FROM CONTRACT_TYPES
GO
/****** Object:  StoredProcedure [dbo].[GetEmployeeByRole]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetEmployeeByRole]
@Role varchar(30) = NULL
AS
SELECT E.*
FROM EMPLOYEES E
WHERE @Role IS NULL OR E.[Role] = @Role
GO
/****** Object:  StoredProcedure [dbo].[GetEmployeeContracts]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetEmployeeContracts]
@EmployeeID int
AS

SELECT C.ContractType, StartDate, EndDate, Salary, HoursPerWeek
FROM CONTRACTS C INNER JOIN CONTRACT_TYPES CT
ON C.ContractType = CT.ContractType
WHERE EmployeeID = @EmployeeID
GO
/****** Object:  StoredProcedure [dbo].[GetEmployeeData]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetEmployeeData]
@EmployeeID int
AS

SELECT *
FROM EMPLOYEES
WHERE EmployeeID = @EmployeeID
GO
/****** Object:  StoredProcedure [dbo].[GetEmployeeRoleList]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetEmployeeRoleList]
@WithDescription bit = 0
AS
IF @WithDescription = 0
	SELECT R.Name
	FROM ROLES R;
ELSE
	SELECT *
	FROM ROLES;
GO
/****** Object:  StoredProcedure [dbo].[GetEmployeeVacations]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetEmployeeVacations]
@EmployeeID int
AS

SELECT StartDate, EndDate, Paid
FROM VACATIONS
WHERE EmployeeID = @EmployeeID
GO
/****** Object:  StoredProcedure [dbo].[GetFaults]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetFaults]
@OnlyUnsolved BIT = 0
AS

IF @OnlyUnsolved = 0
	SELECT *
	FROM FAULTS
	ORDER BY DangerLevel

ELSE
	SELECT *
	FROM FAULTS F
	WHERE F.SolvedDate IS NULL
	ORDER BY DangerLevel
GO
/****** Object:  StoredProcedure [dbo].[GetMapEdges]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetMapEdges]
AS

SELECT N1.PosX AS AX, N1.PosY AS AY, N2.PosX AS BX, N2.PosY AS [BY]
FROM ADJECENCIES A
JOIN NODES N1 ON N1.NodeID = A.NodeA
JOIN NODES N2 ON N2.NodeID = A.NodeB





GO
/****** Object:  StoredProcedure [dbo].[GetMapNodes]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetMapNodes]
AS

SELECT N.PosX, N.PosY
FROM NODES N







GO
/****** Object:  StoredProcedure [dbo].[GetMapPlaces]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetMapPlaces]
AS

SELECT PosX,PosY,Width,Height,Name,Entrance, [Description], PlaceCategory
FROM PLACES







GO
/****** Object:  StoredProcedure [dbo].[GetPlaceByCategory]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPlaceByCategory]
@Category varchar(30) = NULL
AS
SELECT P.PlaceID, P.[Name], P.Available, P.PlaceCategory
FROM PLACES P
WHERE @Category IS NULL OR P.PlaceCategory = @Category
GO
/****** Object:  StoredProcedure [dbo].[GetPlaceCategories]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPlaceCategories]
@WithDescription bit = 0
AS
IF @WithDescription = 0
	SELECT C.[Name]
	FROM PLACE_CATEGORIES C

ELSE
	SELECT C.[Name], C.[Description]
	FROM PLACE_CATEGORIES C
GO
/****** Object:  StoredProcedure [dbo].[GetRemainingTicketUses]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetRemainingTicketUses]
@TicketID int
AS
SELECT T.UsesLeft
FROM TICKETS T
WHERE T.TicketID = @TicketID
GO
/****** Object:  StoredProcedure [dbo].[GetSubscriberID]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetSubscriberID]
	@Email varchar(30),
	@Password varchar(32) = NULL --Not Used
AS
BEGIN
	SELECT S.SubscriberID
	FROM SUBSCRIBERS S
	WHERE S.Email = @Email --Password not used
END
GO
/****** Object:  StoredProcedure [dbo].[GetTaskByEmployee]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetTaskByEmployee]
@EmployeeID int
AS
SELECT T.TaskID AS TaskID,T.TaskDescription AS TaskDescription, T.[Priority] AS [Priority], P.[Name] AS [Name],T.FaultID, F.[Description] AS [Description]
FROM TASKS T
JOIN WORKS W ON W.TaskID = T.TaskID
LEFT JOIN PLACES P ON P.PlaceID = T.PlaceID
LEFT JOIN FAULTS F ON F.FaultID = T.FaultID
WHERE W.EmployeeID = @EmployeeID
ORDER BY T.[Priority];
GO
/****** Object:  StoredProcedure [dbo].[GetTicketClasses]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetTicketClasses]
AS
BEGIN
	SELECT *
	FROM TICKET_CLASSES TC
END
GO
/****** Object:  StoredProcedure [dbo].[GetTicketsBySubscriberID]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetTicketsBySubscriberID]
	@SubscriberID int
AS
BEGIN
		SELECT TC.[Type], TC.Duration, T.UsesLeft, TC.MaxUses, (SELECT MAX(UD.UseDate) FROM USE_DATES UD WHERE UD.TicketID = T.TicketID) AS LastUsed
		FROM TICKETS T
		JOIN PURCHASES P ON T.TicketID = P.TicketID
		JOIN TICKET_CLASSES TC ON TC.TicketClassID = T.TicketClassID
		WHERE P.SubscriberID = @SubscriberID
END
GO
/****** Object:  StoredProcedure [dbo].[GetTimetableAltered]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetTimetableAltered]
@PlaceID int,
@GivenDate date
AS
SELECT *
	FROM SCHEDULES_ALTERED SA
	WHERE SA.PlaceID = @PlaceID AND SA.[Date] = @GivenDate
GO
/****** Object:  StoredProcedure [dbo].[GetTimetableByPlaceAndDate]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetTimetableByPlaceAndDate]
@PlaceID int,
@GivenDate date
AS
IF NOT EXISTS (SELECT *
	FROM SCHEDULES_ALTERED SA
	WHERE SA.PlaceID = @PlaceID AND SA.[Date] = @GivenDate)

	SELECT S.OpeningTime AS OpeningTime, S.ClosingTime AS ClosingTime
	FROM SCHEDULES S
	WHERE S.PlaceID = @PlaceID AND S.[Weekday] = DATENAME(dw, @GivenDate)

ELSE
	SELECT SA.OpeningTime AS OpeningTime, SA.ClosingTime AS ClosingTime
	FROM SCHEDULES_ALTERED SA
	WHERE SA.PlaceID = @PlaceID AND SA.[Date] = @GivenDate
GO
/****** Object:  StoredProcedure [dbo].[GetTimetableStandard]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetTimetableStandard]
@PlaceID int,
@Weekday varchar(10)
AS
SELECT *
	FROM SCHEDULES S
	WHERE S.PlaceID = @PlaceID AND S.[Weekday] = @Weekday


GO
/****** Object:  StoredProcedure [dbo].[HireEmployee]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[HireEmployee]
@Name varchar(20),
@Surname varchar(20),
@Email varchar(30),
@Gender char(1),
@BirthDate date,
@Telephone varchar(11),
@Role varchar(30),
@Username varchar(20),
@PasswordHash varchar(64)
AS
DECLARE @EmpID int
DECLARE @EmpIDTable IDTableType
BEGIN TRANSACTION insEmp

INSERT INTO EMPLOYEES(Name, Surname, Email, BirthDate, Gender, TelephoneNumber, Role)
OUTPUT Inserted.EmployeeID INTO @EmpIDTable
VALUES (@Name, @Surname, @Email, @BirthDate, @Gender, @Telephone, @Role);

SET @EmpID = (SELECT * FROM @EmpIDTable);

INSERT INTO ACCOUNTS(EmployeeID, UserName, PasswordHash)
VALUES (@EmpID, @Username, @PasswordHash);

COMMIT TRANSACTION insEmp
GO
/****** Object:  StoredProcedure [dbo].[InsertConcession]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertConcession]
@CompanyID int,
@PlaceID int,
@StartDate date,
@EndDate date
AS

INSERT INTO CONCESSIONS(CompanyID, PlaceID, StartDate, EndDate)
VALUES (@CompanyID, @PlaceID, @StartDate, @EndDate)
GO
/****** Object:  StoredProcedure [dbo].[InsertContract]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertContract]
@EmployeeID int,
@ContractType varchar(30),
@Salary float,
@StartDate date,
@EndDate date
AS
INSERT INTO CONTRACTS(EmployeeID, ContractType, StartDate, EndDate, Salary)
VALUES (@EmployeeID, @ContractType, @StartDate, @EndDate, @Salary)

GO
/****** Object:  StoredProcedure [dbo].[InsertExternalCompany]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertExternalCompany]
@Name varchar(50),
@Representative varchar(40),
@Telephone varchar(11)
AS

INSERT INTO EXTERNAL_COMPANIES (Name, Representative, TelephoneNumber)
VALUES (@Name, @Representative, @Telephone)
GO
/****** Object:  StoredProcedure [dbo].[InsertFault]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertFault]
@DangerLevel int,
@Description varchar(500),
@StartDate date,
@PlaceID int

AS

SET XACT_ABORT ON
BEGIN TRANSACTION insertfault

INSERT INTO FAULTS
VALUES (@DangerLevel, @Description, @StartDate, NULL, @PlaceID)

COMMIT TRANSACTION insertfault
GO
/****** Object:  StoredProcedure [dbo].[InsertSubscriber]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertSubscriber]
@Name varchar(20),
@Surname varchar(20),
@Email varchar(30),
@Gender char(1),
@BirthDate date
AS
INSERT INTO SUBSCRIBERS ([Name], Surname, Email, Gender, BirthDate, SubscriptionDate)
OUTPUT Inserted.SubscriberID
VALUES (@Name, @Surname, @Email, @Gender, @BirthDate, GETDATE())
GO
/****** Object:  StoredProcedure [dbo].[InsertTicket]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertTicket]
@TicketClassID int,
@SubscriberID int = NULL,
@NewTicketID int OUTPUT
AS
SET XACT_ABORT ON
BEGIN TRANSACTION ticketTrans

DECLARE @ins TABLE([ticketID] int)

INSERT INTO TICKETS (TicketClassID, UsesLeft)
OUTPUT Inserted.TicketID INTO @ins
SELECT @TicketClassID, MaxUses
FROM TICKET_CLASSES
WHERE TicketClassID = @TicketClassID;

SET @NewTicketID = (SELECT ticketID FROM @ins);

IF @SubscriberID IS NOT NULL
BEGIN
	INSERT INTO PURCHASES(TicketID, SubscriberID)
	VALUES (@NewTicketID, @SubscriberID)
END

COMMIT TRANSACTION ticketTrans;
GO
/****** Object:  StoredProcedure [dbo].[InsertTimetable]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertTimetable]
@PlaceID int,
@Weekday varchar(10),
@OpeningTime time,
@ClosingTime time
AS

INSERT INTO [dbo].[SCHEDULES]
           ([PlaceID]
           ,[Weekday]
           ,[OpeningTime]
           ,[ClosingTime])
     VALUES
           (@PlaceID
           ,@Weekday
           ,@OpeningTime
           ,@ClosingTime)


GO
/****** Object:  StoredProcedure [dbo].[InsertTimetableVariation]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertTimetableVariation]
@PlaceID int,
@Date date,
@OpeningTime time,
@ClosingTime time
AS

INSERT INTO [dbo].[SCHEDULES_ALTERED]
           ([PlaceID]
           ,[Date]
           ,[OpeningTime]
           ,[ClosingTime])
     VALUES
           (@PlaceID
           ,@Date
           ,@OpeningTime
           ,@ClosingTime)


GO
/****** Object:  StoredProcedure [dbo].[InsertVacations]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertVacations]
@EmployeeID int,
@StartDate date,
@EndDate date,
@Paid bit
AS
INSERT INTO VACATIONS (EmployeeID, StartDate, EndDate, Paid)
VALUES (@EmployeeID, @StartDate, @EndDate, @Paid)
GO
/****** Object:  StoredProcedure [dbo].[LoginEmployee]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[LoginEmployee]
@Username varchar(20),
@PasswordHash varchar(64)

AS

SELECT E.EmployeeID, E.Role
FROM ACCOUNTS A INNER JOIN EMPLOYEES E
ON A.EmployeeID = E.EmployeeID
WHERE A.UserName = @Username AND A.PasswordHash = @PasswordHash
AND (E.Role = 'Dirigente' OR EXISTS (
	SELECT C.*
	FROM CONTRACTS C
	WHERE C.EmployeeID = E.EmployeeID
	AND StartDate <= CONVERT (date, GETDATE())
	AND (EndDate IS NULL
	OR CONVERT (date, GETDATE()) <= EndDate)))
GO
/****** Object:  StoredProcedure [dbo].[SolveFault]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SolveFault]
@FaultID int
AS

SET XACT_ABORT ON
BEGIN TRANSACTION solveFault
BEGIN
	UPDATE FAULTS
	SET FAULTS.SolvedDate = GETDATE()
	WHERE FAULTS.FaultID = @FaultID

	DELETE FROM WORKS
	WHERE WORKS.TaskID IN (SELECT TaskID FROM TASKS WHERE FaultID = @FaultID);

	DELETE FROM TASKS
	WHERE FaultID = @FaultID;
END

DECLARE @PlaceID int;
SET @PlaceID = (SELECT @PlaceID FROM FAULTS WHERE FaultID = @FaultID);
IF NOT EXISTS (
	SELECT *
	FROM FAULTS
	WHERE FaultID = @FaultID
	AND DangerLevel = 0
	AND SolvedDate = NULL)
BEGIN
	UPDATE PLACES
	SET Available = 1
	WHERE PlaceID = @PlaceID;
END

COMMIT TRANSACTION solveFault
GO
/****** Object:  StoredProcedure [dbo].[UseTicket]    Script Date: 10/07/2018 17:06:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UseTicket]
@TicketID int
AS
DECLARE @nUtilizzi int;

SET @nUtilizzi = (SELECT T.UsesLeft
					FROM TICKETS T
					WHERE T.TicketID = @TicketID);

IF @nUtilizzi > 0
BEGIN 
	SET XACT_ABORT ON;
	BEGIN TRANSACTION useTicketTrans;

		ALTER TABLE USE_DATES 
		NOCHECK CONSTRAINT Check_Use_Date_Consistency;

		INSERT INTO USE_DATES
		(TicketID, UseDate) VALUES (@TicketID, GETDATE());

		SET @nUtilizzi -= 1;

		UPDATE TICKETS
		SET UsesLeft = @nUtilizzi
		WHERE TICKETS.TicketID = @TicketID;

		ALTER TABLE USE_DATES 
		WITH CHECK CHECK CONSTRAINT Check_Use_Date_Consistency;

	COMMIT TRANSACTION useTicketTrans;
	RETURN @nUtilizzi;
END








GO
