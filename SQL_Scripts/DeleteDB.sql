USE [lunaparkdb]
GO
/****** Object:  StoredProcedure [dbo].[UseTicket]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[UseTicket]
GO
/****** Object:  StoredProcedure [dbo].[SolveFault]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[SolveFault]
GO
/****** Object:  StoredProcedure [dbo].[LoginEmployee]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[LoginEmployee]
GO
/****** Object:  StoredProcedure [dbo].[InsertVacations]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[InsertVacations]
GO
/****** Object:  StoredProcedure [dbo].[InsertTimetableVariation]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[InsertTimetableVariation]
GO
/****** Object:  StoredProcedure [dbo].[InsertTimetable]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[InsertTimetable]
GO
/****** Object:  StoredProcedure [dbo].[InsertTicket]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[InsertTicket]
GO
/****** Object:  StoredProcedure [dbo].[InsertSubscriber]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[InsertSubscriber]
GO
/****** Object:  StoredProcedure [dbo].[InsertFault]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[InsertFault]
GO
/****** Object:  StoredProcedure [dbo].[InsertExternalCompany]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[InsertExternalCompany]
GO
/****** Object:  StoredProcedure [dbo].[InsertContract]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[InsertContract]
GO
/****** Object:  StoredProcedure [dbo].[InsertConcession]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[InsertConcession]
GO
/****** Object:  StoredProcedure [dbo].[HireEmployee]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[HireEmployee]
GO
/****** Object:  StoredProcedure [dbo].[GetTimetableStandard]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetTimetableStandard]
GO
/****** Object:  StoredProcedure [dbo].[GetTimetableByPlaceAndDate]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetTimetableByPlaceAndDate]
GO
/****** Object:  StoredProcedure [dbo].[GetTimetableAltered]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetTimetableAltered]
GO
/****** Object:  StoredProcedure [dbo].[GetTicketsBySubscriberID]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetTicketsBySubscriberID]
GO
/****** Object:  StoredProcedure [dbo].[GetTicketClasses]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetTicketClasses]
GO
/****** Object:  StoredProcedure [dbo].[GetTaskByEmployee]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetTaskByEmployee]
GO
/****** Object:  StoredProcedure [dbo].[GetSubcriberID]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetSubcriberID]
GO
/****** Object:  StoredProcedure [dbo].[GetRemainingTicketUses]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetRemainingTicketUses]
GO
/****** Object:  StoredProcedure [dbo].[GetPlaceCategories]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetPlaceCategories]
GO
/****** Object:  StoredProcedure [dbo].[GetPlaceByCategory]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetPlaceByCategory]
GO
/****** Object:  StoredProcedure [dbo].[GetMapPlaces]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetMapPlaces]
GO
/****** Object:  StoredProcedure [dbo].[GetMapNodes]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetMapNodes]
GO
/****** Object:  StoredProcedure [dbo].[GetMapEdges]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetMapEdges]
GO
/****** Object:  StoredProcedure [dbo].[GetFaults]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetFaults]
GO
/****** Object:  StoredProcedure [dbo].[GetEmployeeVacations]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetEmployeeVacations]
GO
/****** Object:  StoredProcedure [dbo].[GetEmployeeRoleList]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetEmployeeRoleList]
GO
/****** Object:  StoredProcedure [dbo].[GetEmployeeData]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetEmployeeData]
GO
/****** Object:  StoredProcedure [dbo].[GetEmployeeContracts]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetEmployeeContracts]
GO
/****** Object:  StoredProcedure [dbo].[GetEmployeeByRole]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetEmployeeByRole]
GO
/****** Object:  StoredProcedure [dbo].[GetContractTypes]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetContractTypes]
GO
/****** Object:  StoredProcedure [dbo].[GetConcessions]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetConcessions]
GO
/****** Object:  StoredProcedure [dbo].[GetCompanies]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetCompanies]
GO
/****** Object:  StoredProcedure [dbo].[GetBuildingContracts]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetBuildingContracts]
GO
/****** Object:  StoredProcedure [dbo].[GetAvailableEmployeesByRole]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[GetAvailableEmployeesByRole]
GO
/****** Object:  StoredProcedure [dbo].[FireEmployee]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[FireEmployee]
GO
/****** Object:  StoredProcedure [dbo].[DeleteTimetableVariation]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[DeleteTimetableVariation]
GO
/****** Object:  StoredProcedure [dbo].[DeleteTimetable]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[DeleteTimetable]
GO
/****** Object:  StoredProcedure [dbo].[CreateNewTask]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[CreateNewTask]
GO
/****** Object:  StoredProcedure [dbo].[CompleteTask]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[CompleteTask]
GO
/****** Object:  StoredProcedure [dbo].[AssignTaskToEmployee]    Script Date: 09/07/2018 11:38:45 ******/
DROP PROCEDURE [dbo].[AssignTaskToEmployee]
GO
ALTER TABLE [dbo].[WORKS] DROP CONSTRAINT [FKWor_TAS]
GO
ALTER TABLE [dbo].[WORKS] DROP CONSTRAINT [FKWor_EMP]
GO
ALTER TABLE [dbo].[VACATIONS] DROP CONSTRAINT [FKHasVacation]
GO
ALTER TABLE [dbo].[USE_DATES] DROP CONSTRAINT [FKUtilization]
GO
ALTER TABLE [dbo].[TICKETS] DROP CONSTRAINT [FKTClass]
GO
ALTER TABLE [dbo].[TASKS] DROP CONSTRAINT [FKR_5]
GO
ALTER TABLE [dbo].[TASKS] DROP CONSTRAINT [FKLocation]
GO
ALTER TABLE [dbo].[SCHEDULES_ALTERED] DROP CONSTRAINT [FK__SCHEDULES__Place__324172E1]
GO
ALTER TABLE [dbo].[SCHEDULES] DROP CONSTRAINT [FK__SCHEDULES__Place__314D4EA8]
GO
ALTER TABLE [dbo].[PURCHASES] DROP CONSTRAINT [FKPur_TIC]
GO
ALTER TABLE [dbo].[PURCHASES] DROP CONSTRAINT [FKPur_SUB]
GO
ALTER TABLE [dbo].[PLACES] DROP CONSTRAINT [FKR_4]
GO
ALTER TABLE [dbo].[PLACES] DROP CONSTRAINT [FKEntrance]
GO
ALTER TABLE [dbo].[FAULTS] DROP CONSTRAINT [FKSource]
GO
ALTER TABLE [dbo].[EMPLOYEES] DROP CONSTRAINT [FKR_3]
GO
ALTER TABLE [dbo].[CONTRACTS] DROP CONSTRAINT [FKHiring]
GO
ALTER TABLE [dbo].[CONTRACTS] DROP CONSTRAINT [FKCType]
GO
ALTER TABLE [dbo].[CONCESSIONS] DROP CONSTRAINT [FKUnderContract]
GO
ALTER TABLE [dbo].[CONCESSIONS] DROP CONSTRAINT [FKAgreement]
GO
ALTER TABLE [dbo].[BUILDING_CONTRACTS] DROP CONSTRAINT [FKR_1]
GO
ALTER TABLE [dbo].[BUILDING_CONTRACTS] DROP CONSTRAINT [FKR]
GO
ALTER TABLE [dbo].[ADJECENCIES] DROP CONSTRAINT [FKLeft]
GO
ALTER TABLE [dbo].[ADJECENCIES] DROP CONSTRAINT [FKAdj_NOD]
GO
ALTER TABLE [dbo].[ACCOUNTS] DROP CONSTRAINT [FKLogin_FK]
GO
/****** Object:  Table [dbo].[WORKS]    Script Date: 09/07/2018 11:38:46 ******/
DROP TABLE [dbo].[WORKS]
GO
/****** Object:  Table [dbo].[VACATIONS]    Script Date: 09/07/2018 11:38:46 ******/
DROP TABLE [dbo].[VACATIONS]
GO
/****** Object:  Table [dbo].[USE_DATES]    Script Date: 09/07/2018 11:38:47 ******/
DROP TABLE [dbo].[USE_DATES]
GO
/****** Object:  Table [dbo].[TICKETS]    Script Date: 09/07/2018 11:38:48 ******/
DROP TABLE [dbo].[TICKETS]
GO
/****** Object:  Table [dbo].[TICKET_CLASSES]    Script Date: 09/07/2018 11:38:48 ******/
DROP TABLE [dbo].[TICKET_CLASSES]
GO
/****** Object:  Table [dbo].[TASKS]    Script Date: 09/07/2018 11:38:48 ******/
DROP TABLE [dbo].[TASKS]
GO
/****** Object:  Table [dbo].[SUBSCRIBERS]    Script Date: 09/07/2018 11:38:49 ******/
DROP TABLE [dbo].[SUBSCRIBERS]
GO
/****** Object:  Table [dbo].[SCHEDULES_ALTERED]    Script Date: 09/07/2018 11:38:49 ******/
DROP TABLE [dbo].[SCHEDULES_ALTERED]
GO
/****** Object:  Table [dbo].[SCHEDULES]    Script Date: 09/07/2018 11:38:50 ******/
DROP TABLE [dbo].[SCHEDULES]
GO
/****** Object:  Table [dbo].[ROLES]    Script Date: 09/07/2018 11:38:50 ******/
DROP TABLE [dbo].[ROLES]
GO
/****** Object:  Table [dbo].[PURCHASES]    Script Date: 09/07/2018 11:38:51 ******/
DROP TABLE [dbo].[PURCHASES]
GO
/****** Object:  Table [dbo].[PLACES]    Script Date: 09/07/2018 11:38:51 ******/
DROP TABLE [dbo].[PLACES]
GO
/****** Object:  Table [dbo].[PLACE_CATEGORIES]    Script Date: 09/07/2018 11:38:52 ******/
DROP TABLE [dbo].[PLACE_CATEGORIES]
GO
/****** Object:  Table [dbo].[NODES]    Script Date: 09/07/2018 11:38:52 ******/
DROP TABLE [dbo].[NODES]
GO
/****** Object:  Table [dbo].[FAULTS]    Script Date: 09/07/2018 11:38:53 ******/
DROP TABLE [dbo].[FAULTS]
GO
/****** Object:  Table [dbo].[EXTERNAL_COMPANIES]    Script Date: 09/07/2018 11:38:53 ******/
DROP TABLE [dbo].[EXTERNAL_COMPANIES]
GO
/****** Object:  Table [dbo].[EMPLOYEES]    Script Date: 09/07/2018 11:38:54 ******/
DROP TABLE [dbo].[EMPLOYEES]
GO
/****** Object:  Table [dbo].[CONTRACTS]    Script Date: 09/07/2018 11:38:54 ******/
DROP TABLE [dbo].[CONTRACTS]
GO
/****** Object:  Table [dbo].[CONTRACT_TYPES]    Script Date: 09/07/2018 11:38:55 ******/
DROP TABLE [dbo].[CONTRACT_TYPES]
GO
/****** Object:  Table [dbo].[CONCESSIONS]    Script Date: 09/07/2018 11:38:55 ******/
DROP TABLE [dbo].[CONCESSIONS]
GO
/****** Object:  Table [dbo].[BUILDING_CONTRACTS]    Script Date: 09/07/2018 11:38:55 ******/
DROP TABLE [dbo].[BUILDING_CONTRACTS]
GO
/****** Object:  Table [dbo].[ADJECENCIES]    Script Date: 09/07/2018 11:38:56 ******/
DROP TABLE [dbo].[ADJECENCIES]
GO
/****** Object:  Table [dbo].[ACCOUNTS]    Script Date: 09/07/2018 11:38:56 ******/
DROP TABLE [dbo].[ACCOUNTS]
GO
/****** Object:  UserDefinedFunction [dbo].[EquTaskCheck]    Script Date: 09/07/2018 11:38:56 ******/
DROP FUNCTION [dbo].[EquTaskCheck]
GO
/****** Object:  UserDefinedFunction [dbo].[CheckTicketUsesConsistency]    Script Date: 09/07/2018 11:38:56 ******/
DROP FUNCTION [dbo].[CheckTicketUsesConsistency]
GO
/****** Object:  UserDefinedFunction [dbo].[CheckNullableDateConsistency]    Script Date: 09/07/2018 11:38:56 ******/
DROP FUNCTION [dbo].[CheckNullableDateConsistency]
GO
/****** Object:  UserDefinedFunction [dbo].[CheckDateConsistency]    Script Date: 09/07/2018 11:38:56 ******/
DROP FUNCTION [dbo].[CheckDateConsistency]
GO
/****** Object:  UserDefinedFunction [dbo].[CheckContractsConsistency]    Script Date: 09/07/2018 11:38:56 ******/
DROP FUNCTION [dbo].[CheckContractsConsistency]
GO
/****** Object:  UserDefinedFunction [dbo].[CheckClassUsesConsistency]    Script Date: 09/07/2018 11:38:56 ******/
DROP FUNCTION [dbo].[CheckClassUsesConsistency]
GO
/****** Object:  UserDefinedTableType [dbo].[IDTableType]    Script Date: 09/07/2018 11:38:57 ******/
DROP TYPE [dbo].[IDTableType]
GO
