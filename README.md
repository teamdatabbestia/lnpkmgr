# README #

In questo documento è possibile trovare le istruzioni e le specifiche minime necessarie per il software 
LunaParkApp, un software gestionale per parchi tematici di qualunque dimensione.

### What is this repository for? ###

* Questo repo contiene tutti i sorgenti e gli script necessari a far funzionare un database gestionale 
e un software che ne fa uso, che fa da tramite tra l'end user (dipendenti e clienti del parco) e la base di dati stessa.
* Versione del software: 1.0.0

### How do I get set up? ###

Per il setup customizzato è sufficiente seguire le istruzioni, mentre per una demo andare alla sezione [DEMO](#markdown-header-demo)

#### 1. Configurazione applicativo
+ Per poter aprire il progetto con Visual Studio è necessaria una versione dell'IDE che supporti ambiente .NET (C#) versione 4 o superiore, 
e che abbia installate le librerie LINQ (ADO.NET) e WPF (libreria grafica). Dopo aver correttamente installato e configurato l'IDE Visual Studio è sufficiente aprire il file .sln contenuto in /Application/LunaParkApp

*Di default il progetto è connesso ad un database hostato su internet, preconfezionato, le cui credenziali sono salvate in chiaro, e che non supporta crittografia o sistemi di sicurezza.
Chiediamo pertanto di non aprofittarne per effettuare un uso malevolo, quanto piuttosto di usarlo come demo per un esecuzione rapida dell'applicativo. È possibile connettere l'applicativo 
ad un qualunque altro database con le stesse caratteristiche (elencate sucessivamente) per usarne un'istanza personale.*

#### 2. Configurazione database
+ L'applicazione può funzionare solo se connessa ad un database gestisto dal DBMS SQL Server, che va a sua volta configurato opportunamente. È pertanto necessaria l'installazione di SQL Server
(la più recente andrà bene) sulla macchina locale o hostata su una macchina connessa ad Internet. Una volta configurata e terminata l'installazione, è sufficiente creare un nuovo database ed
eseguire lo script GenerateDB.sql (situato in /SQL_Scripts) per creare ogni struttura, funzione e tabella necessaria all'applicativo. Questo è possibile solamente usando un altro programma Microsoft, SQL Server Management
Studio, installato separatamente.

**Nota importante:** Le funzioni a valori scalari scritte in TSQL (l'SQL di Microsoft SQL Server) possono dover essere create con script non batch (che eseguono una singola istruzione CREATE). Se questo
è il caso, sarà necessario estrapolare i segmenti CREATE FUNCTION dal GenerateDB.sql per eseguirli uno per volta, singolarmente.

+ Alla fine del processo di creazione del DB sarà necessario inserire manualmente almeno un account "dirigente" (con i massimi privilegi) per poter inserire altri account dall'applicativo.
Fatto ciò rimane solo da impostare la connessione al database da Visual Studio editando il dbml (file adibito alla connessione col database)

*Il dbml preconfezionato contiene i riferimenti a tutte le Stored Procedures che la demo del database supporta. Bisogna fare in modo da mantenerle tutte prima di eseguire l'applicativo altrimenti
quest'ultimo non funzionerà. L'interfaccia di Visual Studio permetterà di reimportarle dal DB nel caso venissero eliminate*

+ Infine, se tutti i passaggi sono stati completati correttamente, sarà possibile compilare eseguire e utilizzare il software C#.

#### 3. Dipendenze
- Visual Studio 2015/2017 o sucessivi con ambiente .NET 4 o sucessivi configurato per supportare C#, LINQ e ADO.NET e WPF
- Istanza del database generata con il GenerateDB.sql accessibile tramite internet o locale alla macchina che contiene Visual Studio e opportunamente connessa alla soluzione
- Non ci sono specifiche rilevanti in merito alla macchina che conterrà il DB, mentre il software è stato sviluppato per PC

#### 3. Distribuzione
- Se correttamente configurato, Visual Studio confezionerà le credenziali di accesso al database insieme all'appplicativo, che potrà quindi essere eseguito da più macchine anche senza l'uso di Visual Studio.
- In quest'ultimo caso sarà necessaria un'istanza del database connessa da una rete accessibile dai vari client

### Contribution guidelines ###

* L'applicazione è stata testata nei limiti di test manuali e di un database con pochi record, quindi potrebbe presentare problemi con situazioni estreme (moli di dati troppo grandi, dati particolari, 
connessione alla rete debole)
* Il codice è stato formattato secondo le regole (non troppo restrittive) di Microsoft (default C#), mentre non esiste un uniformità per i new line
* L'applicazione non fa uso di multithreading per avere una GUI responsive, ma questo dettaglio è facilmente risolvibile se fosse necessario (si consultino le API C# per la concorrenza)

### Who do I talk to? ###

* Il progetto è stato sviluppato da Francesco Dente, Samuele Burattini e Luca Deluigi per il corso di Basi di Dati (Giugno/Luglio 2018)
* L'interfaccia e il readme sono in italiano mentre il codice e il database sono programmati interamente in inglese


## DEMO
Per eseguire la demo del software, è sufficiente effettuare una corretta importazione della soluzione (file .sln) come indicato in **Configurazione applicativo** ed eseguire.