﻿using LunaparkApp.Model;
using LunaparkApp.View;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LunaparkApp
{
    /// <summary>
    /// Logica di interazione per PopupNotifySupervisor.xaml
    /// </summary>
    public partial class PopupNotifySupervisor : Window
    {
        private int faultID;
        public PopupNotifySupervisor(int faultid)
        {
            this.faultID = faultid;
            InitializeComponent();
            try
            {
                this.cmbSupervisor.ItemsSource = LunaparkDBModel.Db.GetAvailableEmployeesByRole("Supervisore", System.DateTime.Now.Date);
            }
            catch (SqlException ex)
            {
                ExceptionHandler.ExitWithError(ex);
            }
        }

        private void BtnNotify_Click(object sender, RoutedEventArgs e)
        {
            GetAvailableEmployeesByRoleResult r = (GetAvailableEmployeesByRoleResult)this.cmbSupervisor.SelectedItem;
            if (r != null)
            {
                try
                {
                    int? newID = -1;
                    string FaultName = LunaparkDBModel.Db.GetFaults(true)
                                                            .Where(res => res.FaultID == this.faultID)
                                                            .Select(ris => ris.Description).FirstOrDefault();
                    LunaparkDBModel.Db.CreateNewTask("Elimina il fault " + FaultName, (int)PriorityLevelEnum.Low, null, null, ref newID);
                    if (newID != -1)
                    {
                        LunaparkDBModel.Db.AssignTaskToEmployee(r.EmployeeID, newID);
                    }
                }
                catch (SqlException ex)
                {
                    ExceptionHandler.ExitWithError(ex);
                }
                finally
                {
                    this.Close();
                }
            }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}
