﻿using LunaparkApp.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LunaparkApp
{
    /// <summary>
    /// Logica di interazione per DirectorView.xaml
    /// </summary>
    public partial class DirectorView : UserControl
    {
        public DirectorView()
        {
            InitializeComponent();
        }

        private void BtnEmployeeManagement_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.SetView(AppViewID.EmployeeManagement);
        }

        private void BtnConcessionsBuilding_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.SetView(AppViewID.Concessions);
        }
    }
}
