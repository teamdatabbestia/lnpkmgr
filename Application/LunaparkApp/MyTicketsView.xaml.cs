﻿using LunaparkApp.Model;
using LunaparkApp.View;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LunaparkApp
{
    /// <summary>
    /// Logica di interazione per MyTicketsView.xaml
    /// </summary>
    public partial class MyTicketsView : UserControl
    {
        public MyTicketsView()
        {
            InitializeComponent();
            if (GlobalData.currentLoggedSubscriberID.HasValue)
            {
                try
                {
                    this.DtgCustomerMyTickets.ItemsSource = LunaparkDBModel.Db.GetTicketsBySubscriberID(GlobalData.currentLoggedSubscriberID.Value).Select(tck => new
                    {
                        Tipo = tck.Type,
                        Durata = tck.Duration,
                        Utilizzi = tck.UsesLeft + "/" + tck.MaxUses,
                        Ultimo = tck.LastUsed.HasValue ? tck.LastUsed.Value.ToString() : "Mai"
                    });
                }
                catch (SqlException ex)
                {
                    ExceptionHandler.ExitWithError(ex);
                }
            }
        }
    }
}
