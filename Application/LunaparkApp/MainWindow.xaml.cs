﻿using LunaparkApp.Model;
using LunaparkApp.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LunaparkApp
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static MainWindow Instance { get; set; }

        private static Stack<AppViewID> viewStack = new Stack<AppViewID>();
        public MainWindow()
        {
            InitializeComponent();
            Instance = this;
            SetView(0);
        }

        public static void SetView(AppViewID viewID, bool noStack = false)
        {
            MainWindow.Instance.DataContext = new MainWindowViewModel(viewID);
            if (!noStack)
            {
                viewStack.Push(viewID);
            }
        }

        public static void Back()
        {
            if (viewStack.Count <= 1)
            {
                Application.Current.Shutdown();
            }
            else
            {
                viewStack.Pop();
                MainWindow.Instance.DataContext = new MainWindowViewModel(viewStack.Peek());
            }
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            if (GlobalData.currentWhishListTicketClassID.HasValue)
            {
                MessageBoxResult result = MessageBox.Show("Stato: " + (GlobalData.currentLoggedSubscriberID.HasValue ? "Loggato" : "Non loggato") + "\nAnnullare l'acquisto del biglietto corrente?",
                                      "Conferma",
                                      MessageBoxButton.YesNo,
                                      MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    GlobalData.currentWhishListTicketClassID = null;
                    Back();
                }
            }
            else
            {
                Back();
            }
        }
    }
}
