﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LunaparkApp.Model;
using LunaparkApp.Utils;
using LunaparkApp.View;

namespace LunaparkApp
{
    /// <summary>
    /// Logica di interazione per SupervisorView.xaml
    /// </summary>
    public partial class SupervisorView : UserControl
    {
        private List<FaultDescrID> faultList;

        public SupervisorView()
        {
            InitializeComponent();
            this.CmbPriority.ItemsSource = Enum.GetValues(typeof(PriorityLevelEnum));
            try
            {
                CmbPlace.ItemsSource = LunaparkDBModel.Db.GetPlaceByCategory(null);
                List<string> roleList = LunaparkDBModel.Db.GetEmployeeRoleList(false).Select(r => r.Name).ToList();
                roleList.Add("Tutti");
                cmbRole.ItemsSource = roleList;
                faultList = LunaparkDBModel.Db.GetFaults(true).Select(r => new FaultDescrID
                {
                    FaultID = r.FaultID,
                    Description = r.Description,
                    Place = r.PlaceID
                }).ToList();

            }
            catch (SqlException ex)
            {
                ExceptionHandler.ExitWithError(ex);
            }
            CmbFault.ItemsSource = faultList;
        }



        private bool checkForm()
        {
            return !(this.CmbPriority.SelectedItem == null
                || this.TxtDescr.Text == ""
                || this.DtgEmp.SelectedItems.Count == 0);
        }

        private void btnAddTask_Click(object sender, RoutedEventArgs e)
        {
            if (checkForm())
            {
                int? taskID = null;
                int? faultID = CmbFault.SelectedItem != null ? (int?)((FaultDescrID)CmbFault.SelectedItem).FaultID : null;
                int? placeID = CmbPlace.SelectedItem != null ? (int?)((GetPlaceByCategoryResult)CmbPlace.SelectedItem).PlaceID : null;
                try
                {
                    LunaparkDBModel.Db.CreateNewTask(
                        TxtDescr.Text,
                        (int)(PriorityLevelEnum)CmbPriority.SelectedItem,
                        placeID,
                        faultID,
                        ref taskID);
                }
                catch (SqlException ex)
                {
                    ExceptionHandler.SqlExceptionMessage(ex);
                    return;
                }
                if (taskID != null)
                {
                    try
                    {
                        foreach (GetAvailableEmployeesByRoleResult r in DtgEmp.SelectedItems)
                        {
                            LunaparkDBModel.Db.AssignTaskToEmployee(r.EmployeeID, taskID);
                        }
                    }
                    catch (SqlException ex)
                    {
                        ExceptionHandler.ExitWithError(ex);
                    }
                }
                MessageBox.Show("I task sono stati inseriti e assegnati ai dipendenti selezionati",
                                  "Operazione completata con successo",
                                  MessageBoxButton.OK,
                                  MessageBoxImage.Information);

            }
            else
            {
                MessageBox.Show("Inserisci tutti i parametri prima di continuare",
                                  "Errore",
                                  MessageBoxButton.OK,
                                  MessageBoxImage.Error);
            }
        }

        private void cmbRole_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string selectedRole = ((string)cmbRole.SelectedItem);
            try
            {
                if (selectedRole == "Tutti")
                {
                    DtgEmp.ItemsSource = LunaparkDBModel.Db.GetAvailableEmployeesByRole(null, System.DateTime.Now);
                }
                else
                {
                    DtgEmp.ItemsSource = LunaparkDBModel.Db.GetAvailableEmployeesByRole(selectedRole, System.DateTime.Now);
                }
            }
            catch (SqlException ex)
            {
                ExceptionHandler.ExitWithError(ex);
            }
        }

        private void CmbPlace_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.CmbFault.ItemsSource = faultList.Where(f => f.Place == ((GetPlaceByCategoryResult)this.CmbPlace.SelectedItem).PlaceID);
        }

        private void BtnTimeChange_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.SetView(View.AppViewID.InsertTimetables);
        }
    }

    class FaultDescrID
    {
        public int FaultID { get; set; }
        public string Description { get; set; }
        public int Place { get; set; }
        public override string ToString()
        {
            return FaultID + " " + Description;
        }
    }
}
