﻿using LunaparkApp.Model;
using LunaparkApp.View;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LunaparkApp
{
    /// <summary>
    /// Logica di interazione per EmployeeGeneralView.xaml
    /// </summary>
    public partial class EmployeeGeneralView : UserControl
    {
        private static ISet<string> validRoleSet = new HashSet<string>(new string[] { "Supervisore", "Cassiere" });
        public EmployeeGeneralView()
        {
            InitializeComponent();
            this.Refresh();
            if (validRoleSet.Contains(GlobalData.currentLoggedEmployeeRole))
            {
                this.BtnRole.Content = GlobalData.currentLoggedEmployeeRole;
                this.BtnRole.Visibility = Visibility.Visible;
            }
            else
            {
                this.BtnRole.Visibility = Visibility.Hidden;
            }

        }

        private void BtnComplete_Click(object sender, RoutedEventArgs e)
        {
            int TaskID = ((GetTaskByEmployeeResult)DtgTasks.SelectedItem).TaskID;
            int? fault = ((GetTaskByEmployeeResult)DtgTasks.SelectedItem).FaultID;
            try
            {
                LunaparkDBModel.Db.CompleteTask(TaskID, GlobalData.currentLoggedEmployeeID);
                if (fault != null)
                {
                    MessageBoxResult r = MessageBox.Show("Vuoi risolvere anche il fault collegato a questo task?",
                                                    "Invia converma",
                                                    MessageBoxButton.YesNo,
                                                    MessageBoxImage.Question);
                    if (r == MessageBoxResult.Yes)
                    {
                        PopupNotifySupervisor p = new PopupNotifySupervisor((int)fault);
                        p.ShowDialog();
                    }
                }
                this.Refresh();
            }
            catch (SqlException ex)
            {
                ExceptionHandler.SqlExceptionMessage(ex);
            }

        }

        private void Refresh()
        {
            try
            {
                this.DtgTasks.ItemsSource = LunaparkDBModel.Db.GetTaskByEmployee(GlobalData.currentLoggedEmployeeID);
            }
            catch (SqlException ex)
            {
                ExceptionHandler.ExitWithError(ex);
            }
        }

        private void BtnFaults_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.SetView(View.AppViewID.FaultsManaging);
        }

        private void BtnRole_Click(object sender, RoutedEventArgs e)
        {
            switch (GlobalData.currentLoggedEmployeeRole)
            {
                case "Supervisore": MainWindow.SetView(View.AppViewID.Supervisor); break;
                case "Cassiere": MainWindow.SetView(View.AppViewID.Cashier); break;
                default: break;
            }
        }
    }
}
