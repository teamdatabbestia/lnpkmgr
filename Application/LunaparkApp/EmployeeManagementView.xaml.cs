﻿using LunaparkApp.Model;
using LunaparkApp.View;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LunaparkApp
{
    /// <summary>
    /// Logica di interazione per EmployeeManagementView.xaml
    /// </summary>
    public partial class EmployeeManagementView : UserControl
    {
        public EmployeeManagementView()
        {
            InitializeComponent();
            this.DtpContractStartDate.SelectedDate = DateTime.Now;
            try
            {
                this.CmbContractType.ItemsSource = LunaparkDBModel.Db.GetContractTypes();
            }
            catch (SqlException ex)
            {
                ExceptionHandler.ExitWithError(ex);
            }
            this.CmbContractType.SelectedIndex = 0;
            this.Refresh();
        }

        private void Refresh()
        {
            try
            {
                this.DtgEmployeeList.ItemsSource = LunaparkDBModel.Db.GetEmployeeByRole(null);
            }
            catch (SqlException ex)
            {
                ExceptionHandler.ExitWithError(ex);
            }
        }

        private void BtnInsertVacations_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LunaparkDBModel.Db.InsertVacations(
                    GetSelectedEmployeeID(),
                    this.DtpVacationStartDate.SelectedDate,
                    this.DtpVacationsEndDate.SelectedDate,
                    this.ChkPaidVacations.IsChecked);
                MessageBox.Show("Inserimento riuscito.",
                    "OK",
                    MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }
            catch (SqlException ex)
            {
                ExceptionHandler.SqlExceptionMessage(ex);
            }
        }

        private void BtnHire_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.SetView(AppViewID.Hiring);
        }

        private void BtnFireSelected_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LunaparkDBModel.Db.FireEmployee(GetSelectedEmployeeID());
                MessageBox.Show("Dipendente licenziato correttamente.",
                    "OK",
                    MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }
            catch (SqlException ex)
            {
                ExceptionHandler.SqlExceptionMessage(ex);
            }
        }

        private void BtnEmployeeInfo_Click(object sender, RoutedEventArgs e)
        {
            GlobalData.currentlyVisualizedEmployeeID = this.GetSelectedEmployeeID();
            MainWindow.SetView(AppViewID.EmployeeInfo);
        }

        private void CmbContractType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GetContractTypesResult selected = (GetContractTypesResult)this.CmbContractType.SelectedItem;
            this.TxtSalary.Text = selected.DefaultSalary.ToString();
        }

        private void BtnInsertContract_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                float salary;
                LunaparkDBModel.Db.InsertContract(
                    GetSelectedEmployeeID(),
                    ((GetContractTypesResult)this.CmbContractType.SelectedItem).ContractType,
                    float.TryParse(TxtSalary.Text, out salary) ? salary : ((GetContractTypesResult)this.CmbContractType.SelectedItem).DefaultSalary,
                    this.DtpContractStartDate.SelectedDate,
                    this.ChkPermanent.IsChecked.Value ? null : this.DtpContractEndDate.SelectedDate);
                MessageBox.Show("Nuovo contratto aggiunto correttamente.",
                    "OK",
                    MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }
            catch (SqlException ex)
            {
                ExceptionHandler.SqlExceptionMessage(ex);
            }
        }

        private int GetSelectedEmployeeID()
        {
            return ((GetEmployeeByRoleResult)this.DtgEmployeeList.SelectedItem).EmployeeID;
        }
    }
}
