﻿using LunaparkApp.Model;
using LunaparkApp.View;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LunaparkApp
{
    /// <summary>
    /// Logica di interazione per TicketClassesView.xaml
    /// </summary>
    public partial class TicketClassesView : UserControl
    {

        private List<GetTicketClassesResult> TcList;
        private int SelectedIndex = 0;
        public TicketClassesView()
        {
            InitializeComponent();
            try
            {
                this.TcList = LunaparkDBModel.Db.GetTicketClasses().ToList();
            }
            catch(SqlException ex)
            {
                ExceptionHandler.ExitWithError(ex);
            }
            this.DtgTicketClasses.ItemsSource = TcList.Select(tc => new { Name = tc.Type, tc.Duration, tc.MaxUses});
            if (TcList.Count > 0)
            {
                this.DtgTicketClasses.SelectedIndex = this.SelectedIndex;
            }
            LoadTicketClassDescription();
        }

        private void DtgTicketClasses_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.SelectedIndex = this.DtgTicketClasses.SelectedIndex;
            LoadTicketClassDescription();
        }

        private void LoadTicketClassDescription()
        {
            if (TcList.Count > 0)
            {
                GetTicketClassesResult current = TcList.ElementAt(this.DtgTicketClasses.SelectedIndex);
                this.LblTicketClassDescription.Content = "Tipo di biglietto: " + current.Type + "\nDurata: " + current.Duration + "\nUtilizzi massimi: " + current.MaxUses + "\nPrezzo: " + current.Price + "€";
            }
            else
            {
                this.LblTicketClassDescription.Content = "Nessun biglietto disponibile";
            }
        }

        private GetTicketClassesResult GetCurrentTicketClass()
        {
            return TcList.Count > 0 ? TcList.ElementAt(this.SelectedIndex) : null;
        }

        private void BtnPurchaseThisClass_Click(object sender, RoutedEventArgs e)
        {
            GetTicketClassesResult current = GetCurrentTicketClass();
            if (current != null)
            {
                if (GlobalData.currentLoggedSubscriberID.HasValue)
                {
                    MessageBoxResult result = MessageBox.Show("Sei sicuro di voler comprare il biglietto corrente?",
                                          "Conferma",
                                          MessageBoxButton.YesNo,
                                          MessageBoxImage.Question);
                    if (result == MessageBoxResult.Yes)
                    {
                        try
                        {
                            int? id;
                            id = null;
                            LunaparkDBModel.Db.InsertTicket(current.TicketClassID, GlobalData.currentLoggedSubscriberID, ref id);
                            if (id != null)
                            {
                                MessageBox.Show("Acquisto avvenuto", "Successo", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }
                        catch (SqlException ex)
                        {
                            ExceptionHandler.SqlExceptionMessage(ex);
                        }
                    }
                    GlobalData.currentWhishListTicketClassID = null;
                }
                else
                {
                    MessageBoxResult result = MessageBox.Show("Per comprare questo biglietto è necessario il login.\nProseguire?",
                                          "Proseguire?",
                                          MessageBoxButton.OKCancel,
                                          MessageBoxImage.Question);
                    if (result == MessageBoxResult.OK)
                    {
                        GlobalData.currentWhishListTicketClassID = current.TicketClassID;
                        MainWindow.SetView(AppViewID.CustomerLogin, true);
                    }
                }
            }
        }
    }
}
