﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunaparkApp.Model
{
    class LunaparkDBModel
    {
        private static LunaparkDBContextDataContext db;
        public static LunaparkDBContextDataContext Db
        {
            get
            {
                if (db == null)
                {
                    db = new LunaparkDBContextDataContext();
                }
                return db;
            }
        }
    }
}
