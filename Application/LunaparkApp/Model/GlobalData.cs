﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunaparkApp.Model
{
    class GlobalData
    {
        public static int? currentLoggedSubscriberID { get; set; }

        public static int? currentWhishListTicketClassID { get; set; }

        public static int? currentLoggedEmployeeID { get; set; }

        public static string currentLoggedEmployeeRole { get; set; }

        public static int currentlyVisualizedEmployeeID { get; set; }
    }
}
