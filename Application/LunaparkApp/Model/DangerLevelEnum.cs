﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunaparkApp.Model
{
    enum DangerLevelEnum
    {
        Danger = 0,
        Caution = 10,
        Warning = 30,
    }
}
