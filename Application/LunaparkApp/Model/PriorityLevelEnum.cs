﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunaparkApp.Model
{
    enum PriorityLevelEnum
    {
        High = 0,
        Medium = 10,
        Low = 100
    }
}
