﻿using LunaparkApp.Model;
using LunaparkApp.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LunaparkApp
{
    /// <summary>
    /// Logica di interazione per CustomerHomeView.xaml
    /// </summary>
    public partial class CustomerHomeView : UserControl
    {
        public CustomerHomeView()
        {
            InitializeComponent();
        }

        private void BtnClientMap_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.SetView(AppViewID.Map);
        }

        private void BtnTicketClasses_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.SetView(AppViewID.TicketClasses);
        }

        private void BtnCustomerLogin_Click(object sender, RoutedEventArgs e)
        {
            if (GlobalData.currentLoggedSubscriberID.HasValue)
            {
                MessageBoxResult result = MessageBox.Show("Un account ha già effettuato l'accesso.\nPremere Yes per proseguire a MyTickets.\nPremere No per riaccedere.\nPremere Cancel per ritornare.",
                                                        "Già Loggato",
                                                        MessageBoxButton.YesNoCancel,
                                                        MessageBoxImage.Information);
                if (result == MessageBoxResult.Yes)
                {
                    MainWindow.SetView(AppViewID.MyTickets);
                }
                else if (result == MessageBoxResult.No)
                {
                    GlobalData.currentLoggedSubscriberID = null;
                    MainWindow.SetView(AppViewID.CustomerLogin);
                }
            }
            else
            {
                MainWindow.SetView(AppViewID.CustomerLogin);
            }
        }

        private void BtnTimetables_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.SetView(AppViewID.Timetables);
        }
    }
}
