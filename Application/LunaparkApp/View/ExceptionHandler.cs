﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LunaparkApp.View
{
    class ExceptionHandler
    {
        public static bool Handle (Exception e)
        {
            if (e is SqlException)
            {
                SqlExceptionMessage(e as SqlException);
                return false;
            } else
            {
                MessageBox.Show("Errore generico: " + e.Message + "\n\nOrigine: " + e.Source + "\n\nIn: " + e.TargetSite + "\n\nStack: " + e.StackTrace,
                   "Unhandled Exception",
                   MessageBoxButton.OK,
                   MessageBoxImage.Error);
                return false;
            }
        }

        public static void SqlExceptionMessage(SqlException e)
        {
            MessageBox.Show("Errore SQL: " + e.Message + "\n\nOrigine: " + e.Source + "\n\nIn: " + e.TargetSite,
                "SQLException",
                MessageBoxButton.OK,
                MessageBoxImage.Warning);
            Console.Error.WriteLine("Errore SQL: " + e.Message + "\n\nOrigine: " + e.Source + "\n\nIn: " + e.TargetSite + "\n\nStack: " + e.StackTrace);
        }

        public static void ExitWithError (Exception e = null)
        {
            Console.Error.WriteLine(e == null ? "Error. Aborting." : e.ToString());
            Application.Current.Shutdown(1);
        }

        public static void ExitWithError(string s)
        {
            Console.Error.WriteLine(s);
            Application.Current.Shutdown(1);
        }
    }
}
