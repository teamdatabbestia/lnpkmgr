﻿namespace LunaparkApp.View
{
    public enum AppViewID
    {
        Home,
        CustomerHome,
        Map,
        CustomerLogin,
        TicketClasses,
        MyTickets,
        EmployeeGeneral,
        EmployeeLogin,
        Timetables,
        FaultsManaging,
        Director,
        EmployeeManagement,
        Hiring,
        Supervisor,
        EmployeeInfo,
        Cashier,
        InsertTimetables,
        Concessions
    }
}