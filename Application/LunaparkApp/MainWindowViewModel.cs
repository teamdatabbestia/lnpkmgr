﻿using LunaparkApp.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunaparkApp
{
    class MainWindowViewModel
    {
        public AppViewID SwitchView { get; set; }

        public MainWindowViewModel(AppViewID viewID)
        {
            SwitchView = viewID;
        }
    }
}
