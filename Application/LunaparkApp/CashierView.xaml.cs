﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LunaparkApp.Model;
using LunaparkApp;
using System.Data.SqlClient;
using LunaparkApp.View;

namespace LunaparkApp
{
    /// <summary>
    /// Logica di interazione per CashierView.xaml
    /// </summary>
    public partial class CashierView : UserControl
    {
        int? ticketClassID = null;

        public CashierView()
        {
            InitializeComponent();
            try
            {
                this.comboBox.ItemsSource = LunaparkDBModel.Db.GetTicketClasses();
            }
            catch (SqlException e)
            {
                ExceptionHandler.ExitWithError(e);
            }
        }

        private void BtnCompra_Click(object sender, RoutedEventArgs e)
        {
            int? newID;
            newID = null;
            if (ticketClassID != null)
            {
                MessageBoxResult r = MessageBox.Show("Sicuro di voler comprare il biglietto?", "Conferma acquisto", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (r == MessageBoxResult.Yes)
                {
                    int? subID = null;
                        if (!string.IsNullOrWhiteSpace(this.TxtEmail.Text))
                        {
                            GetSubscriberIDResult res = LunaparkDBModel.Db.GetSubscriberID(this.TxtEmail.Text, null).FirstOrDefault();
                            if (res == null)
                            {
                                MessageBox.Show("L'email inserita non è valida.", "Errore", MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }
                            subID = res.SubscriberID;
                        }
                    try
                    {
                        LunaparkDBModel.Db.InsertTicket(ticketClassID, subID, ref newID);
                        if (newID != null)
                        {
                            MessageBox.Show("Biglietto acquistato correttamente. Codice biglietto: " + newID, "Operazione completata", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            MessageBox.Show("Il biglietto non è stato registrato", "Errore", MessageBoxButton.OK, MessageBoxImage.Stop);
                        }
                    }
                    catch (SqlException ex)
                    {
                        ExceptionHandler.SqlExceptionMessage(ex);
                    }
                }
            }
            else
            {
                MessageBox.Show("Selezionare il tipo di biglietto", "Errore", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ticketClassID = ((GetTicketClassesResult)this.comboBox.SelectedItem).TicketClassID;
        }

        private void BtnUseTicket_Click(object sender, RoutedEventArgs e)
        {
            if (TxtTicketID.Text != "")
            {
                try
                {
                    int ticket = int.Parse(TxtTicketID.Text);
                    int qr = LunaparkDBModel.Db.UseTicket(ticket);
                    if (qr >= 0)
                    { 
                        MessageBox.Show("Biglietto timbrato, usi rimanent: " + qr, "Operazione completata", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("L'operazione non è andata a buon fine", "Errore", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                catch(Exception)
                {
                    MessageBox.Show("Controllare i dati inseriti", "Errore", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
    }
}
