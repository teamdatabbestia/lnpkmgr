﻿using LunaparkApp.View;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LunaparkApp
{
    /// <summary>
    /// Interaction logic for TimetablesView.xaml
    /// </summary>
    public partial class TimetablesView : UserControl
    {
        private List<GetPlaceByCategoryResult> places;
        public TimetablesView()
        {
            InitializeComponent();
            try
            {
                this.places = Model.LunaparkDBModel.Db.GetPlaceByCategory(null).ToList();
            }
            catch (SqlException ex)
            {
                ExceptionHandler.ExitWithError(ex);
            }
            this.CmbAttractionPicker.ItemsSource = places.Select(e => e.Name);
            this.CmbAttractionPicker.SelectedIndex = 0;
            this.ClnDatePicker.SelectedDate = DateTime.Today;
            updateTimetables();
        }

        private void updateTimetables()
        {
            try
            {
                List<GetTimetableByPlaceAndDateResult> tt = Model.LunaparkDBModel.Db.GetTimetableByPlaceAndDate(
                places.ElementAt(this.CmbAttractionPicker.SelectedIndex).PlaceID,
                this.ClnDatePicker.SelectedDate).ToList();
                if (tt.Count > 0)
                {
                    this.DtgTimetables.ItemsSource = tt.Select(el => new { Apertura = el.OpeningTime, Chiusura = el.ClosingTime });
                }
                else
                {
                    this.DtgTimetables.ItemsSource = new Object[] { new { Orario = "Sempre aperto" } };
                }
            }
            catch (SqlException ex)
            {
                ExceptionHandler.ExitWithError(ex);
            }
        }

        private void CmbAttractionPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            updateTimetables();
        }

        private void updateTimetables(object sender, SelectionChangedEventArgs e)
        {
            updateTimetables();
        }
    }
}
