﻿using LunaparkApp.Model;
using LunaparkApp.View;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LunaparkApp
{
    /// <summary>
    /// Logica di interazione per EmployeeInfoView.xaml
    /// </summary>
    public partial class EmployeeInfoView : UserControl
    {
        public EmployeeInfoView()
        {
            InitializeComponent();
            try
            {
                GetEmployeeDataResult res = LunaparkDBModel.Db.GetEmployeeData(GlobalData.currentlyVisualizedEmployeeID).First();
                string data = "Nome: " + res.Name + "\n";
                data += "Cognome: " + res.Surname + "\n";
                data += "Sesso: " + res.Gender + "\n";
                data += "Data di nascita: " + res.BirthDate.ToString("dd/MM/yyyy") + "\n";
                data += "Telefono: " + res.TelephoneNumber + "\n";
                data += "Email: " + res.Email + "\n";
                data += "Ruolo: " + res.Role + "\n";
                data += "ID: " + res.EmployeeID + "\n";

                this.LblGeneralData.Content = data;

                this.DtgVacations.ItemsSource = LunaparkDBModel.Db.GetEmployeeVacations(GlobalData.currentlyVisualizedEmployeeID);
                this.DtgContracts.ItemsSource = LunaparkDBModel.Db.GetEmployeeContracts(GlobalData.currentlyVisualizedEmployeeID);
            }
            catch (SqlException ex)
            {
                ExceptionHandler.ExitWithError(ex);
            }
        }
    }
}
