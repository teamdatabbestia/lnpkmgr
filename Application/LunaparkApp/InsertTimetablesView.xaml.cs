﻿using LunaparkApp.View;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LunaparkApp
{
    /// <summary>
    /// Interaction logic for InsertTimetablesView.xaml
    /// </summary>
    public partial class InsertTimetablesView : UserControl
    {
        private List<GetPlaceByCategoryResult> places;
        private List<GetTimetableStandardResult> timetables;
        private List<GetTimetableAlteredResult> timetableVariations;

        public InsertTimetablesView()
        {
            InitializeComponent();
            try
            {
                this.places = Model.LunaparkDBModel.Db.GetPlaceByCategory(null).ToList();
            }
            catch (SqlException ex)
            {
                ExceptionHandler.ExitWithError(ex);
            }
            this.CmbAttractions.ItemsSource = places.Select(e => e.Name);
            this.CmbAttractions.SelectedIndex = 0;
            this.ClnDatePicker.SelectedDate = DateTime.Today;
            updateTimetablesGrid();
        }

        private void CmbAttractions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            updateTimetablesGrid();
        }

        private void updateTimetablesGrid()
        {
            if (this.ClnDatePicker.SelectedDate.HasValue && this.CmbAttractions.SelectedItem != null)
            {
                try
                {
                    this.timetables = Model.LunaparkDBModel.Db.GetTimetableStandard(this.places.ElementAt(this.CmbAttractions.SelectedIndex).PlaceID,
                        this.ClnDatePicker.SelectedDate.Value.DayOfWeek.ToString()).ToList();
                    this.timetableVariations = Model.LunaparkDBModel.Db.GetTimetableAltered(this.places.ElementAt(this.CmbAttractions.SelectedIndex).PlaceID,
                        this.ClnDatePicker.SelectedDate.Value).ToList();
                    this.DtgTimetables.ItemsSource = this.timetables.Select(e => new TimePair(e.OpeningTime, e.ClosingTime));
                    this.DtgTimetableVariations.ItemsSource = this.timetableVariations.Select(e => new TimePair(e.OpeningTime, e.ClosingTime));
                }
                catch (SqlException ex)
                {
                    ExceptionHandler.ExitWithError(ex);
                }
            }
        }

        private void updateTimetables(object sender, SelectionChangedEventArgs e)
        {
            updateTimetablesGrid();
        }

        private void BtnDeleteTimetable_Click(object sender, RoutedEventArgs e)
        {
            if (this.ClnDatePicker.SelectedDate.HasValue && this.CmbAttractions.SelectedItem != null)
            {
                try
                {
                    if (this.DtgTimetables.SelectedItem != null)
                    {
                        Model.LunaparkDBModel.Db.DeleteTimetable(this.timetables.ElementAt(this.DtgTimetables.SelectedIndex).PlaceID,
                            this.timetables.ElementAt(this.DtgTimetables.SelectedIndex).Weekday,
                            this.timetables.ElementAt(this.DtgTimetables.SelectedIndex).OpeningTime);
                    }
                    if (this.DtgTimetableVariations.SelectedItem != null)
                    {
                        Model.LunaparkDBModel.Db.DeleteTimetableVariation(this.timetableVariations.ElementAt(this.DtgTimetableVariations.SelectedIndex).PlaceID,
                            this.timetableVariations.ElementAt(this.DtgTimetableVariations.SelectedIndex).Date,
                            this.timetableVariations.ElementAt(this.DtgTimetableVariations.SelectedIndex).OpeningTime);
                    }
                    if (this.DtgTimetableVariations.SelectedItem == null && this.DtgTimetables.SelectedItem == null)
                    {
                        MessageBox.Show("Nessun orario selezionato", "Impossibile cancellare", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                catch (SqlException ex)
                {
                    ExceptionHandler.SqlExceptionMessage(ex);
                }
            }
            updateTimetablesGrid();
        }

        private class TimePair
        {
            public TimeSpan Opening { get; set; }
            public TimeSpan Closing { get; set; }

            public TimePair(TimeSpan open, TimeSpan close)
            {
                this.Opening = open;
                this.Closing = close;
            }
        }

        private void BtnInsertTimetable_Click(object sender, RoutedEventArgs e)
        {
            if (this.ClnDatePicker.SelectedDate.HasValue && this.CmbAttractions.SelectedItem != null)
            {
                try
                {
                    TimePair newTimetable = new TimePair(TimeSpan.Parse(this.TxtOpeningTime.Text), TimeSpan.Parse(this.TxtClosingTime.Text));
                    if (newTimetable.Closing <= newTimetable.Opening)
                    {
                        MessageBox.Show("Errore nell'inserimento di una variazione di orario.\n" + "Il tempo di chiusura deve essere maggiore del tempo di apertura",
                            "Errore inserimento", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    if (this.ChkWeekly.IsChecked.HasValue && this.ChkWeekly.IsChecked.Value)
                    {
                        Model.LunaparkDBModel.Db.InsertTimetable(this.places.ElementAt(this.CmbAttractions.SelectedIndex).PlaceID,
                            this.ClnDatePicker.SelectedDate.Value.DayOfWeek.ToString(),
                            newTimetable.Opening, newTimetable.Closing);
                        MessageBox.Show("Inserimento avvenuto con successo", "OK", MessageBoxButton.OK, MessageBoxImage.Information);
                        updateTimetablesGrid();
                    }
                    else if (this.ChkWeekly.IsChecked.HasValue)
                    {
                        Model.LunaparkDBModel.Db.InsertTimetableVariation(this.places.ElementAt(this.CmbAttractions.SelectedIndex).PlaceID,
                            this.ClnDatePicker.SelectedDate.Value,
                            newTimetable.Opening, newTimetable.Closing);
                        MessageBox.Show("Inserimento avvenuto con successo", "OK", MessageBoxButton.OK, MessageBoxImage.Information);
                        updateTimetablesGrid();
                    }
                }
                catch (SqlException s)
                {
                    ExceptionHandler.SqlExceptionMessage(s);
                }
                catch (FormatException)
                {
                    MessageBox.Show("Formato orario non valido", "Errore inserimento", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                catch (OverflowException)
                {
                    MessageBox.Show("Formato orario non valido", "Errore inserimento", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
    }
}
