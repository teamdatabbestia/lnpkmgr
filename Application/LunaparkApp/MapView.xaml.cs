﻿using LunaparkApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LunaparkApp.Utils;
using System.Data.SqlClient;
using LunaparkApp.View;

namespace LunaparkApp
{
    /// <summary>
    /// Logica di interazione per MapView.xaml
    /// </summary>
    public partial class MapView : UserControl
    {
        private const double MODEL_MAP_WIDTH = 290;
        private const double MODEL_MAP_HEIGHT = 220;
        private const double NODE_SIZE = 5;
        private const string defaultvalue = "Tutti i luoghi";

        private IList<GetPlaceByCategoryResult> PlaceList;

        public MapView()
        {
            InitializeComponent();
            try
            {
                this.CmbPlaceCategory.ItemsSource = Enumerable.Repeat(defaultvalue, 1).Concat(LunaparkDBModel.Db.GetPlaceCategories(false).Select(r => r.Name));
                PlaceList = LunaparkDBModel.Db.GetPlaceByCategory(null).ToList();
            }
            catch (SqlException ex)
            {
                ExceptionHandler.ExitWithError(ex);
            }
            this.DtgMap.ItemsSource = PlaceList;
            this.CmbPlaceCategory.SelectedIndex = 0;
        }

        private void CmbPlaceCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string currentvalue = CmbPlaceCategory.SelectedItem.ToString();
            if (currentvalue == defaultvalue)
            {
                this.DtgMap.ItemsSource = PlaceList;
            }
            else
            {
                this.DtgMap.ItemsSource = PlaceList.Where(p => p.PlaceCategory == currentvalue);
            }
        }

        private void DrawMap()
        {
            try
            {
                LunaparkDBModel.Db.GetMapEdges()
                    .Select(r => CreateMapEdge(r.AX, r.AY, r.BX, r.BY))
                    .ForEach(e => this.CnvMap.Children.Add(e));

                LunaparkDBModel.Db.GetMapPlaces()
                    .Select(r => CreateMapPlace(r.PosX, r.PosY, r.Width, r.Height, r.PlaceCategory))
                    .ForEach(n => this.CnvMap.Children.Add(n));

                LunaparkDBModel.Db.GetMapNodes()
                    .Select(r => CreateMapNode(r.PosX, r.PosY))
                    .ForEach(n => this.CnvMap.Children.Add(n));
            }
            catch (SqlException ex)
            {
                ExceptionHandler.ExitWithError(ex);
            }
        }

        private UIElement CreateMapPlace(double x, double y, double width, double height, string placeCategory)
        {
            double hRatio = this.CnvMap.ActualWidth / MODEL_MAP_WIDTH;
            double vRatio = this.CnvMap.ActualHeight / MODEL_MAP_HEIGHT;
            Rectangle r = new Rectangle();
            r.Width = width * hRatio;
            r.Height = height * vRatio;
            r.SetValue(Canvas.LeftProperty, x * hRatio);
            r.SetValue(Canvas.TopProperty, y * vRatio);
            r.Stroke = Brushes.Black;
            switch (placeCategory)
            {
                case "Attrazione":
                    r.Fill = CreatePlaceBrush(Colors.Aquamarine);
                    break;
                case "Stand":
                    r.Fill = CreatePlaceBrush(Colors.Maroon);
                    break;
                case "Servizi":
                    r.Fill = CreatePlaceBrush(Colors.Gray);
                    break;
                case "Decorazioni":
                    r.Fill = CreatePlaceBrush(Colors.ForestGreen);
                    break;
            }

            return r;
        }

        private Brush CreatePlaceBrush(Color baseColor)
        {
            return new LinearGradientBrush(baseColor, Color.FromArgb(50, baseColor.R, baseColor.G, baseColor.B), 90);
        }

        private UIElement CreateMapNode(double x, double y)
        {
            double hRatio = this.CnvMap.ActualWidth / MODEL_MAP_WIDTH;
            double vRatio = this.CnvMap.ActualHeight / MODEL_MAP_HEIGHT;
            Ellipse e = new Ellipse();
            e.Width = NODE_SIZE * hRatio;
            e.Height = NODE_SIZE * vRatio;
            e.SetValue(Canvas.LeftProperty, (x - NODE_SIZE / 2) * hRatio);
            e.SetValue(Canvas.TopProperty, (y - NODE_SIZE / 2) * vRatio);
            e.Fill = Brushes.Red;
            e.Stroke = Brushes.Black;
            return e;
        }

        private UIElement CreateMapEdge(double x1, double y1, double x2, double y2)
        {
            double hRatio = this.CnvMap.ActualWidth / MODEL_MAP_WIDTH;
            double vRatio = this.CnvMap.ActualHeight / MODEL_MAP_HEIGHT;
            Line l = new Line();
            l.X1 = x1 * hRatio;
            l.X2 = x2 * hRatio;
            l.Y1 = y1 * vRatio;
            l.Y2 = y2 * vRatio;
            l.StrokeThickness = 5;
            l.Stroke = Brushes.Gold;
            return l;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.DrawMap();
        }
    }
}
