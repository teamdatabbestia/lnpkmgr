﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace LunaparkApp.Utils
{
    class PasswordGenerator
    {
        public string GenerateHash(string password)
        {
            SHA256 sha = SHA256.Create();
            byte[] data = sha.ComputeHash(Encoding.ASCII.GetBytes(password));
            return data.Select(b => string.Format("{0:x2}", b)).Aggregate("", (s, c) => s + c);
        }
    }
}
