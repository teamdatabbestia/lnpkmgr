﻿using LunaparkApp.Model;
using LunaparkApp.Utils;
using LunaparkApp.View;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LunaparkApp
{
    /// <summary>
    /// Logica di interazione per HiringView.xaml
    /// </summary>
    public partial class HiringView : UserControl
    {
        private const int PASS_LEN = 8;

        public HiringView()
        {
            InitializeComponent();
            try
            {
                this.CmbRole.ItemsSource = LunaparkDBModel.Db.GetEmployeeRoleList(false).Select(r => r.Name);
            }
            catch (SqlException ex)
            {
                ExceptionHandler.ExitWithError(ex);
            }
        }

        private void BtnHire_Click(object sender, RoutedEventArgs e)
        {
            if (ValidateInput())
            {
                try
                {
                    LunaparkDBModel.Db.HireEmployee(TxtName.Text,
                        TxtSurname.Text,
                        TxtEmail.Text,
                        (char)CmbGender.SelectedItem,
                        DtpBirthDate.SelectedDate,
                        TxtTelephone.Text,
                        CmbRole.SelectedItem.ToString(),
                        TxtUsername.Text,
                        new PasswordGenerator().GenerateHash(TxtPassword.Text));

                    MessageBox.Show("Nuovo dipendente inserito correttamente.",
                        "OK",
                        MessageBoxButton.OK,
                        MessageBoxImage.Information);
                    MainWindow.Back();
                }
                catch (SqlException ex)
                {
                    ExceptionHandler.SqlExceptionMessage(ex);
                }
            }
        }

        private bool ValidateInput()
        {
            if (string.IsNullOrWhiteSpace(TxtUsername.Text))
            {
                ShowInvalidInputMessage("Nome utente non valido.");
                return false;
            }

            if (TxtPassword.Text.Length < PASS_LEN || !TxtPassword.Text.All(c => char.IsLetterOrDigit(c)))
            {
                ShowInvalidInputMessage("Password non valida");
                return false;
            }

            return true;
        }

        private void ShowInvalidInputMessage(string message)
        {
            MessageBox.Show(message,
                    "Input non valido",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
        }
    }
}
