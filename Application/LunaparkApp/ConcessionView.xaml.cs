﻿using LunaparkApp.Model;
using LunaparkApp.View;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LunaparkApp
{
    /// <summary>
    /// Logica di interazione per ConcessionView.xaml
    /// </summary>
    public partial class ConcessionView : UserControl
    {
        public ConcessionView()
        {
            InitializeComponent();
            Refresh();
        }

        private void Refresh()
        {
            try
            {
                IEnumerable<GetCompaniesResult> companies = LunaparkDBModel.Db.GetCompanies().ToArray();
                this.DtgCompanies.ItemsSource = companies.Select(c => new
                {
                    Name = c.Name,
                    Representative = c.Representative,
                    Telephone = c.TelephoneNumber
                });
                this.CmbConcessionCompany.ItemsSource = companies;
                this.CmbConcessionPlace.ItemsSource = LunaparkDBModel.Db.GetPlaceByCategory(null);
                this.DtgConcessions.ItemsSource = LunaparkDBModel.Db.GetConcessions();
                this.DtgBuildingContracts.ItemsSource = LunaparkDBModel.Db.GetBuildingContracts();
            }
            catch (SqlException ex)
            {
                ExceptionHandler.ExitWithError(ex);
            }
        }

        private void BtnInsertCompany_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LunaparkDBModel.Db.InsertExternalCompany(TxtCompanyName.Text, TxtRepresentative.Text, TxtTelephone.Text);
                Refresh();
                MessageBox.Show("Inserimento della compagnia avvenuto.",
                    "OK",
                    MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }
            catch (SqlException ex)
            {
                ExceptionHandler.SqlExceptionMessage(ex);
            }
        }

        private void BtnInsertConcession_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LunaparkDBModel.Db.InsertConcession(
                    (int)CmbConcessionCompany.SelectedValue,
                    (int)CmbConcessionPlace.SelectedValue,
                    this.DtpConcessionStartDate.SelectedDate,
                    this.ChkPermanent.IsChecked.Value ? null : this.DtpConcessionEndDate.SelectedDate);
                Refresh();
                MessageBox.Show("Inserimento avvenuto.",
                    "OK",
                    MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }
            catch (SqlException ex)
            {
                ExceptionHandler.SqlExceptionMessage(ex);
            }
        }
    }
}
