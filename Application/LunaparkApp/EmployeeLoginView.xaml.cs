﻿using LunaparkApp.Model;
using LunaparkApp.Utils;
using LunaparkApp.View;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LunaparkApp
{
    /// <summary>
    /// Logica di interazione per EmployeeLoginView.xaml
    /// </summary>
    public partial class EmployeeLoginView : UserControl
    {

        private PasswordGenerator pwGen = new PasswordGenerator();

        public EmployeeLoginView()
        {
            InitializeComponent();
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            if (CheckLogin())
            {
                if (GlobalData.currentLoggedEmployeeRole == "Dirigente")
                {
                    MainWindow.SetView(AppViewID.Director);
                }
                else
                {
                    MainWindow.SetView(AppViewID.EmployeeGeneral);
                }
            }
            else
            {
                MessageBox.Show("Verifica che il nome utente e la password inserita corrispondano",
                                "Account non trovato",
                                 MessageBoxButton.OKCancel,
                                 MessageBoxImage.Error);
            }
        }

        private bool CheckLogin()
        {
            try
            {
                LoginEmployeeResult res = LunaparkDBModel.Db.LoginEmployee(this.TxtUsername.Text, pwGen.GenerateHash(this.PwbPassword.Password)).FirstOrDefault();
                if (res != null)
                {
                    GlobalData.currentLoggedEmployeeID = res.EmployeeID;
                    GlobalData.currentLoggedEmployeeRole = res.Role;
                    return true;
                }
            }
            catch (SqlException ex)
            {
                ExceptionHandler.SqlExceptionMessage(ex);
            }
            return false;
        }
    }
}
