﻿using LunaparkApp.Model;
using LunaparkApp.View;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LunaparkApp
{
    /// <summary>
    /// Logica di interazione per ClientLoginView.xaml
    /// </summary>
    public partial class CustomerLoginView : UserControl
    {
        public CustomerLoginView()
        {
            InitializeComponent();
            this.CmbCustomerGender.ItemsSource = new char[] { 'E', 'M', 'F' };
        }

        private void BtnCustomerLogin_Click(object sender, RoutedEventArgs e)
        {
            if (this.TxtCustomerEmailLogin.Text.Length < 5 || !IsEmailValid(this.TxtCustomerEmailLogin.Text))
            {
                MessageBox.Show("Indirizzo mail non valido",
                                   "Errore",
                                   MessageBoxButton.OK,
                                   MessageBoxImage.Error);
            }
            else
            {
                try
                {
                    List<GetSubscriberIDResult> SubID = LunaparkDBModel.Db.GetSubscriberID(this.TxtCustomerEmailLogin.Text, null).ToList();
                    if (SubID.Count == 0)
                    {
                        MessageBoxResult result = MessageBox.Show("Account inesistente, iscriviti per continuare.",
                                                                    "Account non trovato",
                                                                    MessageBoxButton.OKCancel,
                                                                    MessageBoxImage.Error);
                        if (result == MessageBoxResult.OK)
                        {
                            this.TxtCustomerName.Focus();
                        }
                    }
                    else if (SubID.Count == 1)
                    {
                        int SubscriberID = SubID.ElementAt(0).SubscriberID;
                        this.LoginID(SubscriberID);
                    }
                    else
                    {
                        throw new InvalidOperationException("Multiple ID found for same email address in database. Aborted.");
                    }
                }
                catch (SqlException ex)
                {
                    ExceptionHandler.SqlExceptionMessage(ex);
                }
            }
        }

        private void BtnCustomerSubscribe_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<GetSubscriberIDResult> SubID = LunaparkDBModel.Db.GetSubscriberID(this.TxtCustomerEmailSubscription.Text, null).ToList();
                if (SubID.Count == 0)
                {
                    if (this.TxtCustomerEmailSubscription.Text.Length < 5 || !IsEmailValid(this.TxtCustomerEmailSubscription.Text))
                    {
                        MessageBox.Show("Indirizzo mail non valido",
                                           "Errore",
                                           MessageBoxButton.OK,
                                           MessageBoxImage.Error);
                    }
                    else if (this.TxtCustomerName.Text.Length == 0)
                    {
                        MessageBox.Show("Nome non valido",
                                           "Errore",
                                           MessageBoxButton.OK,
                                           MessageBoxImage.Error);
                    }
                    else if (this.TxtCustomerSurname.Text.Length == 0)
                    {
                        MessageBox.Show("Cognome non valido",
                                           "Errore",
                                           MessageBoxButton.OK,
                                           MessageBoxImage.Error);
                    }
                    else if (this.CmbCustomerGender.SelectedItem == null)
                    {
                        MessageBox.Show("Nessun genere selezionato",
                                           "Errore",
                                           MessageBoxButton.OK,
                                           MessageBoxImage.Error);
                    }
                    else if (this.DtpCustomerBirthDate.SelectedDate == null)
                    {
                        MessageBox.Show("Nessuna data selezionata",
                                           "Errore",
                                           MessageBoxButton.OK,
                                           MessageBoxImage.Error);
                    }
                    else
                    {
                        try
                        {
                            int? SubscriberID = LunaparkDBModel.Db.InsertSubscriber(this.TxtCustomerName.Text,
                                this.TxtCustomerSurname.Text,
                                this.TxtCustomerEmailSubscription.Text,
                                this.CmbCustomerGender.Text[0],
                                this.DtpCustomerBirthDate.SelectedDate).First().SubscriberID;
                            if (SubscriberID.HasValue)
                            {
                                MessageBox.Show("Iscrizione avvenuta con successo", "Successo", MessageBoxButton.OK, MessageBoxImage.Information);
                                LoginID(SubscriberID.Value);
                            }
                            else
                            {
                                MessageBox.Show("Iscrizione fallita. Qualcosa è andato storto.", "Errore", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                        catch (System.Data.SqlClient.SqlException)
                        {
                            MessageBox.Show("Iscrizione fallita. Qualcosa è andato storto nell'inserimento sul database.", "Errore", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                else if (SubID.Count == 1)
                {
                    MessageBoxResult result = MessageBox.Show("Mail già utilizzata. Accedi per continuare",
                                                                "Mail non disponibile",
                                                                MessageBoxButton.OKCancel,
                                                                MessageBoxImage.Warning);
                    if (result == MessageBoxResult.OK)
                    {
                        this.TxtCustomerEmailLogin.Focus();
                    }
                }
                else
                {
                    throw new InvalidOperationException("Multiple ID found for same email address in database. Aborted.");
                }
            }
            catch (SqlException ex)
            {
                ExceptionHandler.SqlExceptionMessage(ex);
            }
        }

        private bool IsEmailValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private void LoginID(int subscriberID)
        {
            GlobalData.currentLoggedSubscriberID = subscriberID;
            if (GlobalData.currentWhishListTicketClassID.HasValue)
            {
                MessageBoxResult result = MessageBox.Show("Logged in.\nSei sicuro di voler comprare il biglietto corrente?",
                                      "Conferma",
                                      MessageBoxButton.YesNo,
                                      MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    try
                    {
                        int? id;
                        id = null;
                        LunaparkDBModel.Db.InsertTicket(GlobalData.currentWhishListTicketClassID.Value, GlobalData.currentLoggedSubscriberID, ref id);
                        if (id != null)
                        {
                            MessageBox.Show("Acquisto avvenuto", "Successo", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }

                    catch (SqlException ex)
                    {
                        ExceptionHandler.SqlExceptionMessage(ex);
                    }
                }
                GlobalData.currentWhishListTicketClassID = null;
                MainWindow.Back();
            }
            else
            {
                MessageBoxResult confirm = MessageBox.Show("Logged in.\nProcedere alla sezione My Tickets?",
                                          "Logged in",
                                          MessageBoxButton.YesNo,
                                          MessageBoxImage.Question);
                if (confirm == MessageBoxResult.Yes)
                {
                    MainWindow.SetView(AppViewID.MyTickets);
                }
            }
        }

        private void BtnCustomerMyTickets_Click(object sender, RoutedEventArgs e)
        {
            if (GlobalData.currentLoggedSubscriberID.HasValue)
            {
                MainWindow.SetView(AppViewID.MyTickets);
            }
            else
            {
                MessageBox.Show("Nessun accesso effettuato.\nEsegui il Login o Iscriviti per proseguire.", "Non ancora", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }
    }
}
