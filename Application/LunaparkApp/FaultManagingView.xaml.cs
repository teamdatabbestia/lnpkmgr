﻿using System;
using LunaparkApp.Model;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using LunaparkApp.View;

namespace LunaparkApp
{
    /// <summary>
    /// Logica di interazione per FaultManagingView.xaml
    /// </summary>
    public partial class FaultManagingView : UserControl
    {
        private List<GetPlaceByCategoryResult> placeList;
        public FaultManagingView()
        {
            InitializeComponent();
            if (GlobalData.currentLoggedEmployeeRole == "Supervisore")
            {
                this.btnSolve.Visibility = Visibility.Visible;
            }
            else
            {
                this.btnSolve.Visibility = Visibility.Hidden;
            }
            this.CmbDanger.ItemsSource = Enum.GetValues(typeof(DangerLevelEnum));
            try
            {
                this.placeList = LunaparkDBModel.Db.GetPlaceByCategory(null).ToList();
            }
            catch (SqlException ex)
            {
                ExceptionHandler.ExitWithError(ex);
            }
            this.CmbPlace.ItemsSource = placeList;
            this.refresh();
        }

        private void BtnAddFault_Click(object sender, RoutedEventArgs e)
        {
            if (checkForm())
            {
                try
                {
                    LunaparkDBModel.Db.InsertFault((int)this.CmbDanger.SelectedItem, this.TxtDescr.Text, System.DateTime.Now.Date, ((GetPlaceByCategoryResult)this.CmbPlace.SelectedItem).PlaceID);
                    this.refresh();
                }
                catch (SqlException ex)
                {
                    ExceptionHandler.SqlExceptionMessage(ex);
                }
            }
            else
            {
                MessageBox.Show("Inserisci tutti i parametri prima di continuare",
                                   "Errore",
                                   MessageBoxButton.OK,
                                   MessageBoxImage.Error);
            }

        }

        private void btnSolve_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LunaparkDBModel.Db.SolveFault(((FaultRecord)this.DtgFaults.SelectedItem).FaultID);
                refresh();
            }
            catch (SqlException ex)
            {
                ExceptionHandler.SqlExceptionMessage(ex);
            }
        }

        private void refresh()
        {
            try
            {
                this.DtgFaults.ItemsSource = LunaparkDBModel.Db.GetFaults(true).Select(e => new FaultRecord
                {
                    FaultID = e.FaultID,
                    Description = e.Description,
                    DangerLevel = (DangerLevelEnum)e.DangerLevel,
                    Date = e.StartDate.ToString("dd/MM/yyyy"),
                    Place = placeList.Where(p => p.PlaceID == e.PlaceID).FirstOrDefault().Name ?? ""
                });
                this.CmbDanger.SelectedItem = null;
                this.CmbPlace.SelectedItem = null;
                this.TxtDescr.Text = "";
            }
            catch (SqlException ex)
            {
                ExceptionHandler.ExitWithError(ex);
            }
        }

        private bool checkForm()
        {
            return this.CmbPlace.SelectedItem != null && this.CmbDanger.SelectedItem != null && this.TxtDescr.Text != "";
        }
    }

    class FaultRecord
    {
        public int FaultID { get; set; }
        public string Description { get; set; }
        public DangerLevelEnum DangerLevel { get; set; }
        public string Date { get; set; }
        public string Place { get; set; }
    }
}
